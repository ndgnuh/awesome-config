--- unfinished module
--- dragging tiling client will make them float

local awful = require("awful")
local pow = math.pow or function(x, y) return x ^ y end
local ic = require("libs.icecream")
local floating_drag = {
    threshold = 10,
    snap = false,
}

local state = { active = false, x0 = 0, y0 = 0, travel = 0 }


local function drag_done_callback()
    state.active = false
end

function floating_drag.setup(args)
    local threshold = args.threshold or floating_drag.threshold or 10
    local snap = args.snap or floating_drag.snap or false
    floating_drag.threshold = threshold
    floating_drag.snap = snap
end

local function move_callback()
    local coords = mouse.coords()
    ic(coords)
end

function floating_drag.drag(c)
    local coords = mouse.coords()
    local done_callback

    done_callback = function()
        c:disconnect_signal("mouse::move", move_callback)
        c:disconnect_signal("mouse::release", done_callback)
    end
    c:connect_signal("mouse::move", move_callback)
    c:connect_signal("mouse::release", done_callback)

    -- if state.active then
    --     local dx = pow((coords.x or state.x0) - state.x0, 2)
    --     local dy = pow((coords.y or state.y0) - state.y0, 2)
    --     local dist = math.sqrt(dx + dy)
    --     state.x0 = coords.x
    --     state.y0 = coords.y
    --     state.travel = state.travel + dist
    --     if state.travel > state.threshold then
    --         c.floating = true
    --     end
    -- else
    --     state.active = true
    --     state.x0 = coords.x
    --     state.y0 = coords.y
    --     state.travel = 0
    -- end

    -- disable snap...
    c.floating = true
    awful.mouse.client.move(c, floating_drag.snap)
end

return setmetatable(floating_drag, { __call = function(self, c) floating_drag.drag(c) end })
