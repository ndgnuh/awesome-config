-- references:
-- - https://raw.githubusercontent.com/Aire-One/awesome-battery_widget/master/init.lua
-- - https://lazka.github.io/pgi-docs/UPowerGlib-1.0/enums.html#UPowerGlib.DeviceState
-- my custom upower widget
local wibox = require("wibox")
local lgi = require("lgi")
local gears = require("gears")
local beautiful = require("beautiful")
local awful = require("awful")
local cairo = lgi.require("cairo")
local has_upower, upower = pcall(lgi.require, "UPowerGlib")
local ic = require("libs.icecream")
local battery_icon = {}

local function device_state_to_str(state)
    local state_str
    if has_upower then
        local lookup = {
            [upower.DeviceState.PENDING_DISCHARGE] = "Discharging",
            [upower.DeviceState.PENDING_CHARGE]    = "Charging",
            [upower.DeviceState.FULLY_CHARGED]     = "Full",
            [upower.DeviceState.EMPTY]             = "Empty",
            [upower.DeviceState.DISCHARGING]       = "Discharging",
            [upower.DeviceState.CHARGING]          = "Charging",
            [upower.DeviceState.UNKNOWN]           = "Unknown"
        }
        state_str = lookup[state]
    else
        state_str = "UPowerGlib not found"
    end
    return state_str
end

local function getset(attr)
    battery_icon["set_" .. attr] = function(self, value)
        self._private[attr] = value
        self:emit_signal("property::" .. attr)
    end
    battery_icon["get_" .. attr] = function(self, value)
        return self._private[attr]
    end
end

getset("device")
getset("percentage")
getset("client")
getset("charging")
getset("state")

function battery_icon:get_state_string()
    return device_state_to_str(self:get_state())
end

function battery_icon:fit(context, width, height)
    return math.min(width, height)
end

function battery_icon:get_theme()
    return {
        background_normal = "#ababab",
        foreground_normal = "#ffffff"
    }
end

function battery_icon:draw(context, cr, width, height)
    -- information
    local percentage = self:get_percentage()
    local charging = self:get_charging()
    local theme = self:get_theme()
    local state = self:get_state()

    -- metrics
    local w = width
    local h = height / 1.618
    local cw = h / 5  -- horizontal cut out
    local sw = w - cw -- width - cutout


    -- centering
    cr:translate(0, (w - h) / 2)

    -- battery shape clip
    cr:move_to(0, 0)
    cr:rel_line_to(sw, 0)
    cr:rel_line_to(0, cw)
    cr:rel_line_to(cw, 0)
    cr:rel_line_to(0, h - cw - cw)
    cr:rel_line_to(-cw, 0)
    cr:line_to(sw, h - 2)
    cr:line_to(0, h - 2)
    cr:close_path()
    cr:clip()

    -- the empty container
    cr:rectangle(0, 0, w, h)
    cr:set_source(gears.color(theme.background_normal))
    cr:fill()

    -- battery juice
    cr:move_to(0, 0)
    cr:rectangle(0, 0, w * percentage, h)
    cr:set_source(gears.color(theme.foreground_normal))
    cr:fill()
    cr:close_path()

    -- Indicator
    local indicator
    if (
            state == upower.DeviceState.DISCHARGING
            or state == upower.DeviceState.PENDING_DISCHARGE
        ) then
        indicator = tostring(percentage * 100) .. "%"
    elseif (
            state == upower.DeviceState.CHARGING
            or state == upower.DeviceState.PENDING_CHARGE
        ) then
        indicator = tostring("CH")
    elseif state == upower.DeviceState.FULLY_CHARGED then
        indicator = tostring("FULL")
    end

    -- draw indicator
    local font_size = math.ceil(h / 2)
    cr:select_font_face("Sans bold",
        cairo.FontSlant.Normal,
        cairo.FontWeight.BOLD)

    -- text position
    -- ic(cairo.FontWeight)
    cr:set_font_size(font_size)
    local ext = cr:text_extents(indicator)
    local x0 = width / 2 - (ext.width / 2 + ext.x_bearing) - cw / 2
    local y0 = h / 2 - (ext.height / 2 + ext.y_bearing)

    -- fill
    local fill_color = "#212332"
    cr:set_source(gears.color(fill_color))
    cr:move_to(x0, y0)
    cr:show_text(indicator)

    -- stroke
    local stroke_color = theme.foreground_normal
    cr:set_font_size(font_size)
    cr:set_source(gears.color(stroke_color))
    cr:move_to(x0, y0)
    cr:text_path(indicator)
    cr:stroke()
end

function battery_icon:layout(width, height)
    local ret = wibox.widget.base.place_widget_at(self, width / 2, 0, width / 2, height)
    return { ret }
end

local function seconds_to_time(seconds)
    if seconds <= 0 then
        return "00:00:00";
    else
        local hours = string.format("%02.f", math.floor(seconds / 3600));
        local mins = string.format("%02.f", math.floor(seconds / 60 - (hours * 60)));
        local secs = string.format("%02.f", math.floor(seconds - hours * 3600 - mins * 60));
        return hours .. ":" .. mins .. ":" .. secs
    end
end


local function new(args)
    local widget = wibox.widget.base.make_widget()
    gears.table.crush(widget, battery_icon)

    -- display device
    local device = upower.Client():get_display_device()
    widget:set_device(device)

    -- tooltip
    local tooltip = awful.tooltip({})
    tooltip:add_to_object(widget)
    tooltip:set_text("Battery information: Unknown")

    -- update information from UPower
    local function update()
        -- get battery information
        local percentage = device.percentage / 100
        local state = device.state
        local charging = (
            (state == upower.DeviceState.PENDING_CHARGE) or
            (state == upower.DeviceState.CHARGING)
        )

        -- set widget information
        widget:set_percentage(percentage)
        widget:set_charging(charging)
        widget:set_state(state)
        widget:emit_signal("widget::redraw_needed")

        -- set tooltip text
        local seconds_left = seconds_to_time(
            charging and device.time_to_full or device.time_to_empty)
        local tooltip_text = ("∘ Percentage: "
            .. tostring(math.floor(percentage * 100)) .. "%"
            .. "\n"
            .. "∘ Time to " .. (charging and "full" or "empty") .. ": "
            .. seconds_left
            .. "\n"
            .. "∘ Device status: " .. widget:get_state_string()
            .. "\n"
            .. "∘ Discharge rate: " .. device.energy_rate .. "W")
        tooltip:set_text(tooltip_text)
    end


    -- run update once, and watch for changes
    update()
    device.on_notify = update

    return widget
end

return setmetatable(battery_icon, {
    __call = function(self, args) return new(args) end
})
