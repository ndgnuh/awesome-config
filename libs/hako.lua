--- A widget container
--
-- Intended features:
--      - padding
--      - margin
--      - buttons delegate from child
--      - hover-bg/fg
--      - click-bg/fg
local wibox = require("wibox")
local base = require("wibox.widget.base")
local gtable = require("gears.table")
local beautiful = require("beautiful")
local hako = { mt = {} }
local ic = require("libs.icecream")

-- delegate property to children widgets
local function delegate_property(prop, child_id, actual_prop)
    local set_key = "set_" .. prop
    local get_key = "get_" .. prop
    local child_set_key = "set_" .. actual_prop
    local child_get_key = "get_" .. actual_prop
    local prop_signal = "property::" .. prop
    hako[set_key] = function(self, value)
        local child = self:get_children_by_id(child_id)[1]
        child[child_set_key](child, value)
        self:emit_signal("widget::layout_changed")
        self:emit_signal(prop_signal, value)
    end
    hako[get_key] = function(self)
        local child = self:get_children_by_id(child_id)[1]
        return child[child_get_key](child)
    end
end
delegate_property("paddings", "padding_role", "margins")
delegate_property("margins", "margin_role", "margins")
delegate_property("bg", "bg_role", "bg")
delegate_property("widget", "place_role", "widget")

-- extra properties
local function new_property(prop)
    local set_key = "set_" .. prop
    local get_key = "get_" .. prop
    local prop_signal = "property::" .. prop
    hako[set_key] = function(self, value)
        self._private[prop] = value
        self:emit_signal(prop_signal, value)
    end
    hako[get_key] = function(self)
        return self._private[prop]
    end
end
new_property("hover_tint")
new_property("click_tint")

-- layout hako
function hako:layout(context, width, height)
    -- layout the base widget
    -- the root of the base widget is a stack layout, so we use that
    local base_layout = wibox.layout.stack.layout
    local results = base_layout(self, context, width, height)

    -- proceed to layout the child widget at center
    local child = self._private.widget
    local w, h = base.fit_widget(self, context, child, width, height)
    local x = math.max(0, (width - w) / 2)
    local y = math.max(0, (height - h) / 2)
    local ret = base.place_widget_at(child, x, y, w, h)
    table.insert(results, ret)
    return results
end

-- do not overwrite the current base widget
-- just set the children
function hako:set_children(children)
    self._private.widget = children[1]
end

--- create the base widget for hako
function hako:create_base_widget()
    -- create the base widget
    local ret = wibox.widget {
        widget = wibox.layout.stack,
        {
            id = "margin_role",
            widget = wibox.container.margin,
            {
                id = "bg_role",
                widget = wibox.container.background,
                {
                    widget = wibox.container.margin,
                    id = "padding_role",
                }
            }
        },
        {
            widget = wibox.container.background,
            id = "tint_role",
        }
    }

    -- tint color changing
    local hover = false
    local tint = ret:get_children_by_id("tint_role")[1]
    tint:connect_signal("mouse::enter", function(this)
        hover = true
        this.bg = ret:get_hover_tint()
    end)
    tint:connect_signal("mouse::leave", function(this)
        hover = false
        this.bg = nil
    end)
    tint:connect_signal("button::press", function(this) this.bg = ret:get_click_tint() end)
    tint:connect_signal("button::release", function(this) this.bg = hover and ret:get_hover_tint() or nil end)

    return ret
end

--- Declarative widget initialization.
-- @return A box :P
local function new(widget, paddings, bg, hover_tint, click_tint, margins)
    -- create backbone widget
    local ret = hako:create_base_widget()

    -- patch
    gtable.crush(ret, hako, true)

    -- defaults
    ret:set_margins(margins or beautiful.hako_margins or 0)
    ret:set_paddings(paddings or beautiful.hako_paddings or 0)
    ret:set_bg(bg or beautiful.hako_bg or beautiful.bg_normal)
    ret:set_hover_tint(hover_tint or beautiful.hako_hover_tint or "#00000022")
    ret:set_click_tint(click_tint or beautiful.hako_click_tint or "#00000044")
    ret:set_children({ widget })

    return ret
end

-- metamethod
function hako.mt:__call(...)
    return new(...)
end

return setmetatable(hako, hako.mt)
