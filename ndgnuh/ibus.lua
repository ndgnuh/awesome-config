--[[--
Ibus indicator widget for wibar.

The implementation use lgi instead of shell script.
The module also take inspiration from the textclock widget.

@widget ndgnuh.ibus
@see https://lazka.github.io/pgi-docs/IBus-1.0/classes/Bus.html
@see https://lazka.github.io/pgi-docs/IBus-1.0/classes/Engine.html
@see https://lazka.github.io/pgi-docs/IBus-1.0/classes/EngineDesc.html
@usage
wibox.widget {
    widget = require("ndgnuh.ibus"),
    engines = {"xkb:us::eng", "Bamboo"}
    format = "language",
}
--]]


local textbox = require("wibox.widget.textbox")
local awful = require("awful")
local gtable = require("gears.table")
local lgi = require("lgi")
local IBus = lgi.require("IBus")
local ic = require("libs.icecream")

--- Widgets
-- @section 1

--- IBus indicator widget, which is just a text box with predefined
-- buttons, tooltips and update rules.
--
-- When right-clicked, the input method cycles among the configured input methods.
--
-- @table ibus
-- @tfield string format Can be `name`, `longname` or `language`, default is `language`
-- @tfield table engines List of engine name to cycle when right press the widget.
-- @usage
--      wibox.widget {
--          widget = require("ndgnuh.ibus"),
--          engines = {"xkb:us::eng", "Bamboo"}
--          format = "language",
--      }
local ibus = {
    mt = {},
    format = "language"
}

--- Index of an item in a table
-- @lfunction indexof
-- @param t table: the table
-- @param i integer: the item to be searched
local indexof = function(t, i)
    for k, v in ipairs(t) do
        if v == i then return k end
    end
    return 0
end

--- Methods
-- @section Methods

--- Set formatting string
-- @param format Format string, can be `name`, `longname` or `language`
function ibus:set_format(format)
    self._private.format = format
    self:update()
end

--- Get formatting string
--
-- @return string: format string, can be "name", "longname" or "language"
function ibus:get_format(format)
    return self._private.format
end

--- Set ibus widget engine
-- @tparam IBus.EngineDesc engine Engine description
-- @see https://lazka.github.io/pgi-docs/IBus-1.0/classes/EngineDesc.html#IBus.EngineDesc
function ibus:set_engine(engine)
    self._private.engine = engine
    self:update()
end

--- Get return the current ibus engine
-- @see https://lazka.github.io/pgi-docs/IBus-1.0/classes/EngineDesc.html#IBus.EngineDesc
-- @return Ibus.EngineDesc: Engine description.
function ibus:get_engine()
    return self._private.engine
end

--- Update the markup text of the IBus widget.
function ibus:update()
    local format = self._private.format
    local engine = self._private.engine
    if engine then
        local fmt = engine[format]
        self:set_markup(fmt)
    end
end

--- Set list of engines to cycle through when right click
-- @tparam table engines List of engine names (string)
function ibus:set_engines(engines)
    self._private.engines = engines
end

--- Get list of engines to cycle through when right click
-- @treturn table List of engine names (string)
function ibus:get_engines()
    return self._private.engines
end

--- Create a ibus widget
-- This function follows the declarative convention of awesome
-- @tparam string format The format of the widget, can be name, longname, or language
-- @tparam tables engines The list of engines to cycle through
-- @todo Implement tool tips?
-- @todo Context menu for Bamboo?
function ibus.mt:__call(format, engines)
    -- Input validation
    format = format or "name"
    assert(format == "name"
        or format == "longname"
        or format == "language",
        "IBus engine format must be name, longname or language")

    -- Base widget to work on
    local w = textbox("<IBUS>")
    gtable.crush(w, ibus, true)
    w._private.format = format
    w._private.engines = engines

    -- ibus init
    local bus = IBus.Bus.new_async_client()
    bus:set_watch_ibus_signal(true)

    -- update ibus widget when this is connected
    function bus:on_connected()
        local engine = self:get_global_engine()
        ic(engine)
        if engine then
            w:set_engine(engine)
        else
            w:set_engine({ language = "??", name = "??", longname = "??" })
        end
    end

    -- update widget when global engine change
    function bus:on_global_engine_changed()
        local engine = bus:get_global_engine()
        w:set_engine(engine)
    end

    -- setup buttons
    local button_right = awful.button({}, 3, function()
        awful.spawn.raise_or_spawn("ibus-setup")
    end)
    local button_left = awful.button({}, 1, function()
        -- these are engine names, not descriptions
        local engine = bus:get_global_engine().name
        local all_engines = w.engines or { "xkb:us::eng" }

        -- get current engine index and cycle
        local current_idx = indexof(w.engines, engine)
        local next_idx
        if current_idx == #all_engines then
            next_idx = 1
        else
            next_idx = current_idx + 1
        end

        -- switch engine
        local engine_name = all_engines[next_idx]
        awful.spawn.easy_async("ibus engine " .. engine_name, function() end)
    end)
    w:buttons(gtable.join(button_left, button_right))

    -- wrap widget
    return w
end

return setmetatable(ibus, ibus.mt)
