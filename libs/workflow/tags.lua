--- This module define the customized workflow
--
-- The workflow is very similar to the one by gnome, where
-- only one tag is defined by default, other tags are created
-- when they are needed. The created tags are volatile, if no
-- clients are there, they are deleted.
-- This module aim to create most "natural feeling" to each
-- actions as possible.
--
--      require("libs.workflow.tags").setup()
--      local workflow = require("libs.workflow")
--      local key = awful.key({ modkey }, "l", workflow.tags.view_previous_tag)
--
--  @alias m
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local capi = CAPI
local m = {} -- module

--- Get information about current tag
-- @treturn number the current tag index
-- @treturn number the current number of tags
m.tag_metrics = function()
    local screen = awful.screen.focused()
    local tags = screen.tags
    local num_tags = #tags
    local idx = gears.table.hasitem(tags, screen.selected_tag)
    return idx, num_tags
end


--- View the previous tag
-- Implemented as a separate function just in case I wanted to change things
m.view_previous_tag = function()
    awful.tag.viewprev()
end

--- Move client to previous tag
-- If the first tag is selected, does nothing
--- @param view boolean: should the target tag be focused?
m.send_to_previous_tag = function(view)
    local client = capi.client.focus
    if not client then return end
    local i, _ = m.tag_metrics()
    if i <= 1 then return end
    client:move_to_tag(client.screen.tags[i - 1])
    -- view the target tag
    view = view or false
    if view then m.view_previous_tag() end
end

--- Move the current client to next tag
-- If the next tag does not exists, create a new one
-- @tparam boolean view should the target tag be focused?, default false
m.send_to_next_tag = function(view)
    local client = capi.client.focus
    local screen = awful.screen.focused()
    if not client then return end

    -- create new tag if not available
    local i, n = m.tag_metrics()
    if i == n then
        local name = tostring(n + 1)
        local current_tag = screen.selected_tag
        awful.tag.add(name, {
            screen = screen,
            layout = current_tag.layout,
            volatile = true,
        })
    end
    local target_tag = screen.tags[i + 1]
    client:move_to_tag(target_tag)

    -- view the target tag
    view = view or false
    if view then m.view_next_tag() end
end

--- Convenience functions for moving
-- client to previous tag and focus to that tag
m.move_to_previous_tag = function() m.send_to_previous_tag(true) end

--- Convenience functions for moving
-- client to next tag and focus to that tag
m.move_to_next_tag = function() m.send_to_next_tag(true) end

--- View the next tag.
-- If the current tag is the last one, create a volatile tag.
-- The new tag inherit the current tag's layout.
m.view_next_tag = function()
    local screen = awful.screen.focused()
    local i, n = m.tag_metrics()
    if i == n then
        local name = tostring(n + 1)
        local current_tag = screen.selected_tag
        awful.tag.add(name, {
            screen = screen,
            layout = current_tag.layout,
            volatile = true,
        })
    end
    awful.tag.viewnext()
end


--- Show the wallpaper
-- This function toggle between view none and previous tag.
-- When it does, it emits tag history update to notify tag related features
m.view_none_state = { previous = nil }
m.view_none = function()
    local screen = awful.screen.focused()
    local state = m.view_none_state
    if state.previous == nil then
        state.previous = screen.selected_tag
        awful.tag.viewnone()
    else
        awful.tag.viewonly(state.previous)
        state.previous = nil
    end
    capi.screen.emit_signal("tag::history::update", screen)
end


return m
