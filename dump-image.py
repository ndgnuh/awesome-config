import base64
from argparse import ArgumentParser

import cv2
from PIL import Image, ImageOps


def invert_rgba(image):
    r, g, b, a = image.split()
    return Image.merge("RGBA", (b, g, r, a))


parser = ArgumentParser()
parser.add_argument("input")
parser.add_argument("output")
args = parser.parse_args()

image = Image.open(args.input).convert("RGBA")
image = invert_rgba(image)
width = image.width
height = image.height
b64string = base64.b64encode(image.tobytes("raw")).decode("ascii")

lua_template = f"""
local cairo = require("lgi").cairo
local base64 = require("libs.base64")

local data = [===[{b64string}]===]
local width = {width}
local height = {height}
local format = cairo.Format.ARGB32
local stride = cairo.Format.stride_for_width(format, width)
local decoded = base64.decode(data)
local img = cairo.ImageSurface.create_for_data(decoded, format, width, height, stride)

return img
"""
with open(args.output, "w") as f:
    f.write(lua_template)
