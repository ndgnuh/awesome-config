--[[-- Show client toggle-able property

@widget ndgnuh.widgets.client_prop
@usage
local clientprop = require("clientprop")
local widget = wibox.widget {
    widget = clientproperty,
    client = c,
    prop = "floating",
}
--]]
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local client_prop = { mt = {}, _private = {} }
local active_icon = "assets/icons/checkbox-checked.svg"
local inactive_icon = "assets/icons/checkbox-unchecked.svg"
local capi = CAPI

--- The client to update status from
-- @property client
WithProp(client_prop, "client")

--- The property of client to display
-- @property prop
WithProp(client_prop, "prop")

--- The color used when the property is active (true)
-- @property active_color
WithProp(client_prop, "active_color")

--- The color used when the property is inactive (false)
-- @property inactive_color
WithProp(client_prop, "inactive_color")

--- Create the looks of this widget
local template_widget = function()
    local widget = {
        widget = wibox.layout.fixed.horizontal(),
        forced_width = 300,
        forced_height = 32,
        -- icon
        {
            widget = wibox.widget.imagebox,
            id = 'icon',
            image = Icon(inactive_icon, beautiful.fg_normal),
        },

        -- spacer
        {
            widget = wibox.widget.textbox,
            text = '---'
        },

        -- text
        {
            widget = wibox.widget.textbox,
            id = 'text',
            markup = "Hello",
        }
    }
    return widget
end

--- Create a new client_prop widget
function client_prop.mt:__call()
    -- Make widget
    local name = "ndgnuh.widgets.client_prop"
    local proxy = template_widget()
    local widget = wibox.widget(proxy)
    ic({ 1, #widget:get_all_children() })
    gears.table.crush(widget, client_prop, true)
    ic({ 2, #widget:get_all_children() })

    -- Update signals
    --- Update the client prop widget
    local update = function()
        local child_icon = widget:get_all_children()[1]
        local child_text = widget:get_all_children()[3]

        --- Update icon and markup
        local c = self:get_client()
        local prop = self:get_prop()
        local icon, markup, color
        if prop == nil then
            color = self:get_inactive_color() or beautiful.fg_normal
            markup = TextColor("???", color)
            icon = Icon(inactive_icon, color)
        elseif c == nil or not c[prop] then
            color = self:get_inactive_color() or beautiful.fg_normal
            markup = TextColor(prop, color)
            icon = Icon(inactive_icon, color)
        else
            color = self:get_active_color() or beautiful.fg_focus
            markup = TextColor(prop, color)
            icon = Icon(active_icon, color)
        end

        --- Set widget icon and markup
        -- child_icon:set_image(icon)
        child_text:set_markup(markup)
    end

    widget:connect_signal("property::client", update)
    widget:connect_signal("property::prop", update)
    widget:connect_signal("property::active_color", update)
    widget:connect_signal("property::inactive_color", update)

    -- Update from client
    -- widget:connect_signal("property::client", function(c)
    --     local update = function()
    --         ic(widget == nil)
    --         widget:update()
    --     end
    --     c:connect_signal("property::floating", update)
    --     c:connect_signal("property::ontop", update)
    --     c:connect_signal("property::sticky", update)
    --     c:connect_signal("property::maximized", update)
    --     c:connect_signal("property::minimized", update)
    --     c:connect_signal("property::fullscreen", update)
    -- end)
    return widget
end

return setmetatable(client_prop, client_prop.mt)
