------
-- Declarative wrapper for the awful.widget.layoutbox
--
-- Built in layoutbox widget can't be used declaratively in awesome 4.3
-- The widget also at mouse shortcuts and tooltips.
--
-- Buttons: Right click to 
--
-- @widget ndgnuh.layoutbox
-- @usage 
-- wibox.widget {
--      widget = require("ndgnuh.layoutbox")
-- }

local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local capi = CAPI

local layoutbox = { mt = {} }

--- Update the widget with given tag.
-- 
-- This function is used to handle tag related signals.
-- @tag tag AwesomeWM's tag table
function layoutbox:update_from_tag(tag)
    local screen = tag.screen
    local name = awful.layout.getname(awful.layout.get(screen))
    self:set_markup(name)
    self:set_image(beautiful["layout_" .. name])
end

--- Set the layoutbox screen
--
-- When the screen is set, create a new awful.widge.layoutbox
-- and setup that as the child of this widget.
-- @screen screen AwesomeWM screen object.
function layoutbox:set_screen(screen)
    self:set(1, awful.widget.layoutbox(screen))
end

--- Create a new layoutbox
-- @function layoutbox
function layoutbox.mt:__call()
    local wrapped = wibox.widget {
        widget = wibox.layout.fixed.horizontal,
        awful.widget.layoutbox(),
    }
    local widget = wibox.widget.base.make_widget(wrapped)
    return widget
end

return setmetatable(layoutbox, layoutbox.mt)
