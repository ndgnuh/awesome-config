-- this module is 1-way
-- i.e. it cannot receive information from playerctl yet
-- define stub
local m = {}
local call_player = function(meth)
    return function()
        local p = m.get_current_player()
        if p then
            return p[meth](p)
        end
    end
end
m.get_current_player = function() end
m.play = call_player("play")
m.pause = call_player("pause")
m.play_pause = call_player("play_pause")
m.next = call_player("next")
m.prev = call_player("previous")
m.get_album = call_player("get_album")
m.get_artist = call_player("get_artist")
m.get_position = call_player("get_position")
m.get_title = call_player("get_title")

-- load playerctl
local lgi = require("lgi")
local ok, Playerctl = pcall(lgi.require, "Playerctl")
if not Playerctl then
    return m
end

-- replace with a "proper" function
local Player = Playerctl.Player
m.get_current_player = function()
    local player_names = Playerctl.list_players()
    if #player_names == 0 then return end
    local player = Player.new_from_name(player_names[1])
    return player
end

return m
