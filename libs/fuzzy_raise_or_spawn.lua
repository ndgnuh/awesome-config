--- Search clients by class name and raise them, or spawn command
-- This is a "do what I mean" version of `awful.spawn.raise_or_spawn`
local awful = require("awful")
local gears = require("gears")
local capi = { screen = screen, client = client }
local ic = require("libs.icecream")
local fzy = require("libs.fzy")
local m = gears.object({ threshold = 80 })

--- The Levenshtein string distance
-- @param The first string
-- @param The second string
local function lev(a, b)
    local na = #a
    local nb = #b
    if na == 0 then return nb end
    if nb == 0 then return na end

    local taila = a:sub(2, na)
    local tailb = b:sub(2, nb)
    if a:sub(1, 1) == b:sub(1, 1) then
        return lev(taila, tailb)
    else
        local d1 = lev(taila, b)
        local d2 = lev(a, tailb)
        local d3 = lev(taila, tailb)
        return 1 + math.min(d1, d2, d3)
    end
end

function m.get_clients()
    -- Get all clients from all the screen
    -- Do not use the stacked order since it changes every time
    local clients = {}
    for s in capi.screen do
        clients = gears.table.join(clients, s:get_all_clients(false))
    end
    return clients
end

--- Search the client list
--
function m.fuzzy_raise_or_spawn(args)
    local pattern, command
    if type(args) == "string" then
        pattern = args
        command = args
    else
        command = args.command
        pattern = args.pattern or command
    end

    local clients = m.get_clients()
    local candidates = {}
    local candidates_clients = {}

    for _, c in ipairs(clients) do
        if c.type == "normal" then
            table.insert(candidates, c.matcher or c.class)
            table.insert(candidates_clients, c)
        end
    end

    -- fuzzy find
    local matched = fzy.filter(pattern, candidates)

    -- spawn if not found
    -- otherwise raise and focus
    if #matched == 0 then
        awful.spawn(command, {}, function(c)
            c.matcher = pattern
        end)
    else
        local best_idx = matched[1][1]
        local c = candidates_clients[best_idx]

        if c then
            c:emit_signal("request::activate", "fuzzy_matched", { raise = true })
            c.first_tag:view_only()
            client.focus = c
            c:raise()
        end
    end
end

return m.fuzzy_raise_or_spawn
