--- This module defines a wibar widget for managing network.
--
-- @see https://lazka.github.io/pgi-docs/index.html#NM-1.0/classes/Client.html
-- @see https://lazka.github.io/pgi-docs/index.html#NM-1.0/classes/ActiveConnection.html#NM.ActiveConnection
local beautiful = require("beautiful")
local wibox = require("wibox")
local gtable = require("gears.table")
local gtimer = require("gears.timer")
local lgi = require("lgi")
local NM = lgi.require("NM") -- network manager
local lib = require("lib")

local network_indicator = { mt = {} }

--- Proxy function for setting markup
--- @param markup string: widget markup
function network_indicator:set_markup(markup)
    local textbox = self:get_children_by_id('text')[1]
    textbox.markup = markup
end

--- Proxy function for setting icon
--- @param image string: widget icon path or surface
function network_indicator:set_icon(image)
    self:get_children_by_id('icon')[1].image = image
end

--- Update the network indicator markup
-- @tparam table widget the awesomewm widget
-- @tparam lgi.NM.NMClient nmc a network manager client 
network_indicator.update = function(widget, nmc)
    local devices = nmc:get_all_devices()
    local wifi_device = nil
    local ethernet_device = nil
    for _, dev in ipairs(devices) do
        local type = dev:get_device_type()

        -- according to the docs, this is supposed to be
        -- the enum value, not the name of the enum, but
        -- whatever
        if type == "ETHERNET" then
            ethernet_device = dev
        elseif type == "WIFI" then
            wifi_device = dev
        end
    end

    -- Wifi device is available
    local markup = "Unknown"
    local icon = Resource("./assets/icons/wifi_off.svg")
    if wifi_device ~= nil then
        local ap = wifi_device:get_active_access_point()
        if ap ~= nil then
            local ssid = tostring(ap:get_ssid():get_data())
            local strength = ap:get_strength()
            markup = ssid .. " (" .. string.format("%d", strength) .. "%)"
            icon = Icon("./assets/icons/wifi.svg", beautiful.fg_focus)
        else
            markup = "Disconnected"
            icon = Icon("./assets/icons/wifi_off.svg", beautiful.fg_focus)
        end
    elseif ethernet_device ~= nil then -- thernet device available
        -- local speed = ethernet_device:get_speed()
        markup = "Ethernet"
        icon = Icon("./assets/icons/lan.svg", beautiful.fg_focus)
    end

    -- Update widget
    network_indicator.set_markup(widget, markup)
    network_indicator.set_icon(widget, icon)
end

--- Create a new network indicator widget
local new = function(refresh)
    refresh = refresh or 1

    -- Making the base widget
    local widget = wibox.widget {
        layout = wibox.layout.fixed.horizontal,
        spacing = beautiful.xresources.apply_dpi(4),
        {
            id = "icon",
            widget = wibox.widget.imagebox,
            image = Icon("./assets/icons/wifi_off.svg", beautiful.fg_focus),
        },
        {
            id = "text",
            widget = wibox.widget.textbox,
            markup = "Unknown status",
            align = "left",
        },
    }

    -- Create a new NM client for network data
    -- TODO: use async client
    local nm_client = NM.Client.new()
    gtimer.start_new(refresh, function()
        network_indicator.update(widget, nm_client)
        return true
    end)

    -- wrap around base make_widget to make it declarative
    return wibox.widget.base.make_widget(widget)
end

-- Wrap the mt call
function network_indicator.mt:__call(...) return new(...) end

return setmetatable(network_indicator, network_indicator.mt)
