--[[-- Settings menu configuration menu using GTK

@submodule libs.settings
@see https://github.com/lgi-devs/lgi/blob/master/docs/gtk.md
]]
local gears = require("gears")
local lgi = require("lgi")
local Gtk = lgi.require("Gtk", "3.0")
local capi = CAPI

local GridBuilder = { row = 1, col = 1, mt = {} }

function GridBuilder:next_row()
    self.row = self.row + 1
    self.col = 1
end

function GridBuilder:add(widget, span_width, span_height)
    if widget ~= nil then
        self.grid:attach(
            widget,
            self.col,
            self.row,
            span_width or 1,
            span_height or 1
        )
    end
    self.col = self.col + 1
end

function GridBuilder:get()
    return self.grid
end

function GridBuilder.mt:__call(args)
    local grid = Gtk.Grid(args)
    local builder = { grid = grid, row = 1, col = 1 }
    builder = gears.table.crush(builder, GridBuilder, false)
    return builder
end

GridBuilder = setmetatable(GridBuilder, GridBuilder.mt)

local SettingEntry = function(settings, key)
    return Gtk.Entry {
        text = settings.get(key),
        on_changed = function(self)
            settings.set(key, self.text)
        end
    }
end

local RadioGroups = function(args)
    local values = args.values
    local labels = args.labels or args.values
    local current_value = args.current_value or args.values[1]
    local callback = args.callback or function(...) end
    local radios = {}

    for i = 1, #values do
        local radio = Gtk.RadioButton {
            label = labels[i],
            active = (current_value == values[i]),
            group = radios[i - 1],
            on_toggled = function(self)
                if self.active then
                    callback(values[i], i)
                end
            end
        }
        radios[#radios + 1] = radio
    end

    return radios
end

return Stateful(function(state, settings)
    -- Toggle
    if state.win ~= nil then
        state.win:destroy()
        state.win = nil
        return
    end

    local gb = GridBuilder()
    local grid = gb.grid
    local spacing = 12
    grid:set_column_spacing(spacing)
    grid:set_row_spacing(spacing)
    grid:set_column_homogeneous(true)
    grid:set_column_homogeneous(false)
    grid:set_hexpand(Gtk.Align.FILL)

    gb:add(Gtk.Label { halign = Gtk.Align.START, label = "Terminal command" })
    gb:add(SettingEntry(settings, "terminal"))
    gb:next_row()

    gb:add(Gtk.Label { halign = Gtk.Align.START, label = "Calendar command" })
    gb:add(SettingEntry(settings, "calendar"))
    gb:next_row()

    gb:add(Gtk.Label { halign = Gtk.Align.START, label = "Launcher command" })
    gb:add(SettingEntry(settings, "launcher"))
    gb:next_row()

    -- modkey
    local radios = {}
    radios = RadioGroups {
        values = { "Mod1", "Mod4" },
        labels = { "Mod1 (Alt)", "Mod4 (Logo key)" },
        current_value = settings.get("modkey"),
        callback = function(value) settings.set(value, "modkey") end
    }
    gb:add(Gtk.Label { halign = Gtk.Align.START, label = "Modkey (restart required)" })
    gb:add(radios[1])
    gb:next_row()
    gb:add()
    gb:add(radios[2])
    gb:next_row()

    local close_button = Gtk.Button { label = "Close" }
    gb:add(Gtk.Button { label = "Restart", on_clicked = capi.awesome.restart })
    gb:add(close_button)


    state.win = Gtk.ApplicationWindow {
        title = "Settings",
        role = "awesome-popup",
        Gtk.Box {
            orientation = Gtk.Orientation.VERTICAL,
            spacing = 12,
            margin = 12,
            grid,
        }
    }
    state.win:show_all()
    function close_button:on_clicked()
        ic(state.win)
        state.win:destroy()
        state.win = nil
    end
end)
