--[[-- Volume widget

@widget ndgnuh.volume_widget
@alias w
@usage
wibox.widget {
    widget = require("ndgnuh.volume_widget")
}
]]
local wibox = require("wibox")
local gears = require("gears")
local awful = require("awful")
local capi = CAPI
local volume = { mt = {} }

--- Increase volume
-- @button wheelup

--- Decrease volume
-- @button wheeldown

--- Toggle mute
-- @button left


function volume.mt:__call()
    local ret = wibox.widget {
        widget = wibox.widget.textbox,
        markup = "VOL: --",
    }

    -- Buttons
    local spawn = function(cmd)
        return function() awful.spawn.easy_async_with_shell(cmd, function() end) end
    end
    local buttons = gears.table.join(
        awful.button({}, 1, spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),
        awful.button({}, 4, spawn("pactl set-sink-volume @DEFAULT_SINK@ +2.5%")),
        awful.button({}, 5, spawn("pactl set-sink-volume @DEFAULT_SINK@ -2.5%"))
    )
    spawn("pactl set-sink-volume @DEFAULT_SINK@ +0%")() -- trigger first change
    ret:buttons(buttons)


    -- Update
    capi.awesome.connect_signal("volume::changed", function(args)
        if args.mute then
            ret.markup = "MUTED"
        else
            ret.markup = "VOL: " .. tostring(args.volume)
        end
    end)

    return ret
end

return setmetatable(volume, volume.mt)
