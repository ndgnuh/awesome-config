-- https://lazka.github.io/pgi-docs/index.html#LightDM-1/functions.html
local iconbutton = require("libs.iconbutton")
local awful = require("awful")
local lightdm = require("lgi").require("LightDM")
local ic = require("libs.icecream")
local icons = require("icons")

local items = {}
local actions = {
    "shutdown",
    "restart",
    "suspend",
    "hibernate",
}

local action_items = {
    shutdown = { "Shutdown", function() lightdm.shutdown() end, icons.shutdown },
    restart = { "Restart", function() lightdm.restart() end, icons.reboot },
    suspend = { "Suspend", function() lightdm.suspend() end, },
    hibernate = { "Hibernate", function() lightdm.hibernate() end, },
    cancel = { "Cancel", function() end, },
    logout = { "Logout", function() awesome.quit() end, icons.logout, }
}

table.insert(items, action_items.logout)
for _, action in ipairs(actions) do
    if lightdm["get_can_" .. action]() then
        table.insert(items, action_items[action])
    end
end
table.insert(items, action_items.cancel)


local menu = nil

function show()
    if menu == nil then
        menu = awful.menu {
            items = items
        }
    end
    menu:toggle()
end

return show
