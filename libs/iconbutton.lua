local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local ic = require("libs.icecream")

local m = { _mt = {} }

function m:fit(context, width, height)
    -- Find the maximum square available
    local m = math.min(width, height)
    return m, m
end

function m:set_bg(bg) self._private.bg_role.bg = bg end

function m:get_bg() return self._private.bg_role.bg end

function m:set_shape(shape) self._private.bg_role.shape = shape end

function m:get_shape() return self._private.bg_role.shape end

function m:set_normal_bg(bg) self._private.normal_bg = bg end

function m:get_normal_bg() return self._private.normal_bg end

function m:set_hover_bg(bg) self._private.hover_bg = bg end

function m:get_hover_bg() return self._private.hover_bg end

function m:set_active_bg(bg) self._private.active_bg = bg end

function m:get_active_bg() return self._private.active_bg end

function m:set_is_toggle(flag) self._private.is_toggle = flag end

function m:get_is_toggle() return self._private.is_toggle end

function m:set_activated(flag) self._private.activated = flag end

function m:get_activated() return self._private.activated end

function m:set_hovering(flag) self._private.hovering = flag end

function m:get_hovering() return self._private.hovering end

function m:set_icon(icon)
    local icon_role = self._private.icon_role
    icon_role:set_image(icon)
end

function m._mt:__call()
    -- widget creation
    local icon_role = wibox.widget.imagebox()
    local bg_role = wibox.widget {
        widget = wibox.container.background,
        {
            widget = wibox.container.place,
            icon_role,
        },
    }

    local ret = wibox.widget.base.make_widget(bg_role, "libs.icon_button", {
        enable_properties = true,
    })
    ret._private.icon_role = icon_role
    ret._private.bg_role = bg_role
    gears.table.crush(ret, m, true)

    -- base state
    -- TODO: theme
    ret:set_shape(gears.shape.rounded_rect)
    ret:set_normal_bg(gears.color.transparent)
    ret:set_hover_bg(beautiful.primary)
    ret:set_active_bg(beautiful.bg_focus)
    ret:set_activated(false)
    ret:set_hovering(false)
    ret:set_is_toggle(false)

    ret:connect_signal("mouse::enter", function(self)
        self:set_hovering(true)
        local bg = self:get_hover_bg()
        self:set_bg(bg)
    end)
    ret:connect_signal("mouse::leave", function(self)
        self:set_hovering(false)
        local bg
        if self:get_activated() then
            bg = self:get_active_bg()
        else
            bg = self:get_normal_bg()
        end
        self:set_bg(bg)
    end)
    ret:connect_signal("button::press", function(self)
        -- TODO: this is currently broken
        if self:get_is_toggle() then
            local active = self:get_activated()
            self:set_activated(not active)
            if self:set_activated() then
                self:set_bg(self:get_active_bg())
            else
                self:set_bg(self:get_normal_bg())
            end
        else
            self:set_activated(true)
            self:set_bg(self:get_active_bg())
        end
    end)
    ret:connect_signal("button::release", function(self)
        self:set_activated(false)
        if self:get_hovering() then
            self:set_bg(self:get_hover_bg())
        else
            self:set_bg(self:get_normal_bg())
        end
    end)

    return ret
end

return setmetatable(m, m._mt)
