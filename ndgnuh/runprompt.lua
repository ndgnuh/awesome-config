local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local gears = require('gears')
local unpack = table.unpack or unpack
local survival = require("libs.survival")
local lgi = require("lgi")
local ic = require("libs.icecream")

-- arg is just a name
local ctx = survival("ndgnuh-prompt")
local m = {
    widgets = {},
    commands = {},
    matched_commands = {},
    initialized = false,
    selected = 1,
    page = 1,
    per_page = 12,
    keyword = ""
}

--- the string distance
-- @param The first string
-- @param The second string
local function lev(a, b)
    local na = #a
    local nb = #b
    if na == 0 then return nb end
    if nb == 0 then return na end

    local taila = a:sub(2, na)
    local tailb = b:sub(2, nb)
    if a:sub(1, 1) == b:sub(1, 1) then
        return lev(taila, tailb)
    else
        local d1 = lev(taila, b)
        local d2 = lev(a, tailb)
        local d3 = lev(taila, tailb)
        return 1 + math.min(d1, d2, d3)
    end
end

local function ls(dirpath)
    local stdout = io.popen("ls '" .. dirpath .. "'"):read("*a")
    local cmds = gears.string.split(stdout, "\n")
    return cmds
end


-- search for commands matching keywords
m.search = function(keyword)
    local results = {}
    local results_2 = {}
    local lkeyword = keyword:lower()
    local n = #keyword

    for _, cmd in ipairs(m.commands) do
        local lcmd = cmd:lower()

        -- starts with -> best match
        -- just contains -> secondary match
        -- fuzzy matching is too heavy
        if lcmd:sub(1, n) == lkeyword then
            table.insert(results, cmd)
        elseif lcmd:match(lkeyword) then
            table.insert(results_2, cmd)
        end
    end

    -- prevents index starting from 2 when results is empty
    if #results == 0 then
        return results_2
    else
        return gears.table.join(results, results_2)
    end
end

m.init = function()
    if m.initialized then return end

    local theme = beautiful

    -- Widgets
    -- =======

    -- Prompter
    -- --------
    m.widgets.prompt_textbox = wibox.widget.textbox()

    -- Commands
    -- --------
    m.widgets.commands = Map(Range(1, m.per_page), function(i)
        return wibox.widget.textbox("Command " .. tostring(i))
    end)

    -- the command backgrounds & layouts
    m.widgets.command_backgrounds = Map(m.widgets.commands, function(w)
        return wibox.widget {
            widget = wibox.container.background,
            bg = gears.color.transparent,
            w,
        }
    end)

    -- reload button: reload the command lists
    local reload_text = "reload"
    m.widgets.reload_button = wibox.widget.textbox()
    m.widgets.reload_button:set_markup(reload_text)
    m.widgets.reload_button:connect_signal("button::press", function()
        m.reload_path_commands()
    end)
    m.widgets.reload_button:connect_signal("mouse::enter", function()
        m.widgets.reload_button:set_markup(TextColor(reload_text, theme.primary))
    end)
    m.widgets.reload_button:connect_signal("mouse::leave", function()
        m.widgets.reload_button:set_markup(reload_text)
    end)

    -- The main widget
    -- ---------------
    m.widgets.main = wibox.widget {
        widget = wibox.container.background,
        bg = RGBf(0.2, 0.2, 0.22),
        forced_width = 500,
        {
            widget = wibox.container.margin,
            margins = theme.wibar_spacing,
            {
                layout = wibox.layout.fixed.vertical,
                {
                    layout = wibox.layout.align.horizontal,
                    m.widgets.prompt_textbox,
                    nil,
                    m.widgets.reload_button,
                },
                {
                    widget = wibox.container.margin,
                    bottom = theme.wibar_spacing / 2,
                    top = theme.wibar_spacing / 2,
                    {
                        layout = wibox.container.background,
                        bg = theme.primary,
                        forced_height = 2,
                    },
                },
                {
                    layout = wibox.layout.fixed.vertical,
                    unpack(Map(m.widgets.command_backgrounds, function(w)
                        return wibox.widget {
                            widget = wibox.container.margin,
                            top = theme.wibar_spacing / 4,
                            bottom = theme.wibar_spacing / 4,
                            w,
                        }
                    end))
                }
            }
        }
    }

    m.widgets.waiting = {
        widget = wibox.container.background,
        wibox.widget.textbox("Loading..."),
    }

    m.wibox = awful.popup {
        placement = awful.placement.centered,
        widget = m.widgets.main,
        border_width = 2,
        border_color = theme.primary,
    }

    m.load_path_commands()

    m.initialized = true
end

m.reload_path_commands = function()
    m.load_path_commands(true)
    m.set_keyword("")
    return true, false
end

m.load_path_commands = function(clean)
    local commands = ctx.deserialize()
    if clean or commands == nil then
        -- reset commands
        commands = {}
        local pathenv = os.getenv("PATH")
        local paths = gears.string.split(pathenv, ":")
        for _, path in ipairs(paths) do
            commands = gears.table.join(
                ls(path), commands)
        end
        m.commands = commands
        ctx.serialize(m.commands)
    else
        m.commands = commands
    end
end

m.is_visible = function()
    return m.wibox.visible
end

m.hide = function()
    m.wibox.ontop = false
    m.wibox.visible = false
end

m.rerender = function()
    local commands = m.matched_commands

    local offset = (m.page - 1) * m.per_page + 1
    -- out of bounds are displayed as empty
    local offset_end = m.per_page + offset - 1

    -- redraw text boxes
    local i = 1
    for k = offset, offset_end do
        local cmd = commands[k] or "--"
        local w = m.widgets.commands[i]
        w:set_markup(cmd)
        i = i + 1
    end
end

m.rerender_selection = function()
    for i = 1, m.per_page do
        local bg_widget = m.widgets.command_backgrounds[i]
        if i == m.selected then
            bg_widget.bg = RGBf(0.3, 0.3, 0.32)
        else
            bg_widget.bg = gears.color.transparent
        end
    end
end

m.get_total_page = function()
    local nitems = #m.matched_commands
    local pp = m.per_page
    if nitems % pp == 0 then
        return nitems / pp
    else
        return math.ceil(nitems / pp)
    end
end

m.next_selection = function(keyword)
    local next_sel = math.min(m.selected + 1, #m.matched_commands)
    if next_sel > m.per_page then
        next_sel = 1
        m.page = m.page + 1
        m.rerender()
    end
    m.selected = next_sel
    m.rerender_selection()
    return true, false
end

m.prev_selection = function(keyword)
    local n = m.selected - 1
    if n == 0 then
        if m.page == 1 then
            n = 1
        else
            m.page = m.page - 1
            n = m.per_page
            m.rerender()
        end
    end
    m.selected = n
    m.rerender_selection()
    return true, false
end

m.show = function()
    m.wibox.ontop = true
    m.wibox.visible = true
    m.matched_commands = m.commands
    m.rerender()
    m.rerender_selection()

    -- extra keybindings
    local hooks = {
        { {},         "Down", m.next_selection },
        { {},         "Up",   m.prev_selection },
        { { "Mod4" }, "r",    m.reload_path_commands },
        { { "Mod4" }, "j",    m.next_selection },
        { { "Mod4" }, "k",    m.prev_selection },
        { { "Mod4" }, "h", function()
            ic("test")
            return true, false
        end },
    }

    -- run the prompts
    local prompt = m.widgets.prompt_textbox
    m.set_keyword("")
    awful.prompt.run {
        prompt = "<b>Run: </b>",
        text = "",
        textbox = prompt,
        done_callback = m.hide,
        hooks = hooks,
        exe_callback = function(_)
            local idx = m.per_page * (m.page - 1) + m.selected
            local command = m.matched_commands[idx]
            awful.spawn(command, false)
        end,
        changed_callback = function(input)
            m.set_keyword(input)
            m.rerender()
        end,
    }
end

m.set_keyword = function(keyword)
    m.keyword = keyword
    if #keyword == 0 then
        m.matched_commands = m.commands
    else
        m.matched_commands = m.search(keyword)
    end

    m.selected = 1
    m.page = 1
    m.rerender_selection()
end

m.toggle = function()
    if m.is_visible() then
        m.hide()
    else
        m.show()
    end
end

m.run = function()
    m.toggle()
end

m.init()
m.hide()

return m
