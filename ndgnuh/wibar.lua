--[[-- My customized wibar.

@usage
awful.screen.connect_for_each_screen(function(screen)
    require("ndgnuh.wibar").setup { screen = screen }
end)
]]
local awful = require("awful")
local beautiful = require("beautiful")
local layout = require("wibox.layout")
local container = require("wibox.container")
local wibox = require("wibox")
local hako = require("libs.hako")
local tinted = require("libs.tinted")
local import = Import(...)
local upower = require("ndgnuh.widgets.upower")

-- +=================+
-- | Theme variables |
-- +=================+

--- Hover background color
-- @themevar beatiful.hover_bg

--- Hover foreground color
-- @themevar beatiful.hover_fg

--- Clicking foreground color
-- @themevar beatiful.click_fg

--- Clicking background color
-- @themevar beatiful.click_bg

--- Wrap a parent widgets with a child's behaviour
-- More specificly, buttons and tooltips will be transfered
-- @tparam widget parent the parent widget
-- @tparam widget child the child widget, if nil, use the first child (for layout only)
-- @return the parent widget
local wraps = function(parent, child)
    -- Delegate buttons to parent widget
    if IsGitVersion() then
        local buttons = child.buttons
        if #buttons > 0 then
            child.buttons = {}
            parent.buttons = buttons
        end
    else
        local buttons = child:buttons()
        if #buttons > 0 then
            child:buttons({})
            parent:buttons(buttons)
        end
    end
    return parent
end


local menu_item = function(widget_config)
    widget_config = wibox.widget(widget_config)
    local margins = widget_config.margins or 4
    local tooltip = widget_config.tooltip

    -- wrap widgets
    local widget = wibox.widget {
        widget = require("lib.widgets.background"),
        {
            widget = container.margin,
            margins = beautiful.xresources.apply_dpi(margins),
            widget_config,
        }
    }

    --- Delegate buttons to outer widget
    widget = wraps(widget, widget_config)


    if tooltip then
        local tooltip_wb = awful.tooltip({
            margins = beautiful.xresources.apply_dpi(4),
            mode = "outside",
            text = tooltip,
            preferred_positions = { "top" },
        })
        tooltip_wb:add_to_object(widget)
    end

    return widget
end

local function menu_item_system_tray(screen)
    local widget = menu_item { widget = wibox.widget.systray, screen = screen, margins = 2, buttons = {} }
    awesome.connect_signal("custom:toggle-systray", function()
        widget.visible = not widget.visible
    end)
    return widget
end


--- Setup a wibar on a screen.
-- @tparam table args Setup options
-- @tparam table args.screen AwesomeWM screen object
local setup = function(args)
    local screen = args.screen
    local position = args.position or "bottom"

    local wibar = awful.wibar {
        screen = screen,
        position = position,
        stretch = true,
    }

    -- wibar left item
    local left = {
        widget = layout.fixed.horizontal,
        {
            widget = tinted,
            squared = true,
            require("ndgnuh.tagname")()
        },
        {
            widget = tinted,
            squared = true,
            {
                widget = awful.widget.layoutbox,
                screen = screen,
                buttons = awful.button({}, 1, function() awful.layout.inc(1) end),
            },
        },
        require("ndgnuh.tasklist").setup { screen = screen },
    }

    -- wibar right item
    local right = {
        widget = layout.fixed.horizontal,
        spacing = beautiful.xresources.apply_dpi(4),
        {
            widget = tinted,
            require("ndgnuh.widgets.upower"),
        },
        {
            widget = tinted,
            squared = true,
            {
                widget = require("ndgnuh.ibus"),
                format = "language",
                engines = { "xkb:us::eng", "Bamboo", "anthy" },
                id = "ibus"
            },
        },
        -- menu_item {
        --     widget = require("ndgnuh.network_indicator"),
        -- },
        -- menu_item {
        --     widget = require("ndgnuh.volume_widget")
        -- },
        menu_item_system_tray(screen),
        {
            widget = tinted,
            {
                widget = wibox.widget.textclock,
                format = " %H:%M %a, %d/%m/%Y ",
                buttons = awful.button({}, 1, function()
                    local rules = {
                        placement = awful.placement.centered,
                        urgent = true,
                    }
                    awful.spawn.single_instance("gsimplecal", rules)
                end)
            },
        },
    }

    -- wibar setup
    wibar:setup {
        left, nil, right,
        widget = layout.align.horizontal,
        spacing = beautiful.xresources.apply_dpi(8),
    }
end

return { setup = setup }
