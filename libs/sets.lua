local gtable = require("gears.table")

--- Prototype for the set data structure
-- TODO: the set should remember inserting order...
local Set = {
    items = {},                       -- map from hash to items
    hashes = {},                      -- list of hashes
    reverse = {},                     -- reverse map from items to hashes
    hashfn = function(x) return x end -- item hash function
}

function Set:add(item)
    local hash = self.hashfn(item)
    if not self.items[hash] then
        table.insert(self.hashes, hash)
        self.items[hash] = item
    end
end

function Set:contains(item)
    local hash = self.hashfn(item)
    return self.items[hash] ~= nil
end

function Set:remove(item)
    local hash = self.hashfn(item)
    if self.items[hash] ~= nil then
        self.items[hash] = nil

        -- Not very efficient, but whatever
        for i, ref in ipairs(hash) do
            if ref == hash then
                table.remove(self.hashes, i)
                break
            end
        end
    end
end

function Set:iter()
    local i = 1
    local count = #self.hashes
    return function()
        -- Stop iteration
        if i > count then
            return nil
        end

        local hash = self.hashes[i]
        local item = self.items[hash]
        local j = i
        i = i + 1
        return j, item
    end
end

function Set.new(hashfn)
    local set = gtable.clone(Set, true)
    set.hashfn = hashfn or set.hashfn
    return set
end

return Set
