local awful = require("awful")
local m = {}

m.normalize = function(c)
    c.floating             = false
    c.fullscreen           = false
    c.maximized            = false
    c.maximized_vertical   = false
    c.maximized_horizontal = false
end

m.move_to_volatile = function(c, layout)
    layout = layout or awful.layout.suit.max
    local tagname = c.class or c.name or tostring(c)
    local t = awful.tag.add(tagname, {
        screen = c.screen,
        layout = layout,
        volatile = true,
    })
    c:tags { t }
    t:view_only()
end

m.global_focus = function(c)
    if c and c.valid then
        c.first_tag:view_only()
        c.minimized = false
        client.focus = c
        c:raise()
    end
end

return m
