# Reorganization plan (20240112)

## Regarding the `libs` module

- Put lower level functions in the libs module
- The planned modules are
    - `path`: the equivalent of `os.path` module from Python, dealing with files.
    - `table`: the extension to `gears.table`, with more functional programming functions.
    - `fn`: functional programming tools, or just functions in general
    - `icecream`: the icecream module
    - `init`: the "all" namespace for all these functions.
    - `widgets`: the generic widgets wrapper/helper
    - Other extensions to `wibox`, `awful` if needed


These functions are equivalent to `gears`' functions, maybe they can be merged to `AwesomeWM` core.

## Regarding the `widgets` module

This module will only implement widgets that very specific to my workflow.

The planned module are:
    - `res.icons`: icon resources
    - `res.fonts`: font resources
    - The widget submodules
