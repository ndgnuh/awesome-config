local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local iconbutton = require("libs.iconbutton")

local function new()
    -- init
    local icon_file = "assets/icons/wb_incandescent.svg"
    local icon_on = Icon(icon_file, "#fff")
    local icon_off = Icon(icon_file, "#999")
    local icon = wibox.widget {
        widget = iconbutton,
        icon = icon_off
    }

    -- toggle
    local enabled = false
    icon:buttons(awful.button({}, 1, function()
        enabled = not enabled
        if enabled then
            awful.spawn.with_shell("redshift -P -g 1.1:1.1:0.8 -O 5000")
            icon:set_icon(icon_on)
        else
            awful.spawn.with_shell("redshift -x")
            icon:set_icon(icon_off)
        end
    end))

    return icon
end
return new
