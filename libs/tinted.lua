local wibox = require("wibox")
local gtable = require("gears.table")
local beautiful = require("beautiful")
local tinted = { mt = {} }
-- local ic = require("libs.icecream")

function tinted:set_hover_tint(color)
    self._private.hover_tint = color
    self:emit_signal("property::hover_tint", color)
    self:emit_signal("widget::layout_changed")
end

function tinted:set_click_tint(color)
    self._private.click_tint = color
    self:emit_signal("property::click_tint", color)
    self:emit_signal("widget::layout_changed")
end

function tinted:fit(context, width, height)
    if self._private.squared then
        return math.min(width, height)
    else
        return wibox.layout.stack.fit(self, context, width, height)
    end
end

function tinted:set_squared(squared)
    self._private.squared = squared
    self:emit_signal("property::squared", squared)
    self:emit_signal("widget::layout_changed")
end

function tinted:get_squared()
    return self._private.squared
end

function tinted:get_hover_tint()
    return self._private.hover_tint
end

function tinted:get_click_tint()
    return self._private.click_tint
end

function tinted:set_children(children)
    self.place:set_children(children)

    -- delegate buttons if any
    local child = children[1]
    if child then
        if child.get_buttons then
            -- awesome git
            self:set_buttons(children[1]:get_buttons())
        else
            -- awesome 4.3
            self:buttons(children[1]:buttons())
        end
    end
end

local function new(child)
    local tint = wibox.container.background()
    local place = wibox.container.place(child)
    local ret = wibox.widget {
        widget = wibox.layout.stack,
        place,
        tint,
    }
    ret.tint = tint
    ret.place = place
    ret.hover = false
    gtable.crush(ret, tinted, true)

    -- default sets
    ret:set_children({ child })
    ret:set_hover_tint(beautiful.hover_tint_color or "#00000022")
    ret:set_click_tint(beautiful.click_tint_color or "#00000033")

    -- tinted signals
    ret:connect_signal("mouse::enter", function(self)
        self.hover = true
        tint.bg = self:get_hover_tint()
    end)
    ret:connect_signal("mouse::leave", function(self)
        self.hover = false
        tint.bg = nil
    end)
    ret:connect_signal("button::press", function(self)
        tint.bg = self:get_click_tint()
    end)
    ret:connect_signal("button::release", function(self)
        tint.bg = self.hover and self:get_hover_tint() or nil
    end)

    return ret
end

function tinted.mt:__call(...)
    return new(...)
end

return setmetatable(tinted, tinted.mt)
