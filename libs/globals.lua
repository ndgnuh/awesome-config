--- Global functions, because I'm evil.
--
--      require("libs.globals")
local gears = require("gears")
local awful = require("awful")
local lgi = require("lgi")

--- Function functions
--
-- @section fp

--- A value returning "do" block
-- @tparam function closure : The local block to execute.
-- @return Whatever the closure returns
function Do(closure)
    return closure()
end

--- Returns a table with single dispatch based on the first argument
-- Assign type name to this table to use the dispatch, for example
--  f = SingeDispatch()
--  f.string = function(name) print("hello " .. name) end
--  f.number = function(x) return x + 1 end
function SingleDispatch()
    return setmetatable({}, {
        __call = function(self, x, ...)
            local T = type(x)
            local method = self[T]
            assert(method ~= nil, "No method of " .. tostring(self) .. " with type " .. tostring(T))
            return method(x, ...)
        end
    })
end

--- Map
function Map(tbl, fn)
    local ret = {}
    for k, v in pairs(tbl) do
        ret[k] = fn(v)
    end
    return ret
end

--- Range
function Range(a, b)
    local tbl = {}
    for i = a, b do
        table.insert(tbl, i)
    end
    return tbl
end

--- Find index of element in a table, does not work in keys
-- If not found, returns nil.
-- @tparam table haystack : The table  to look in
-- @param needle : The item  to look for
function IndexIn(haystack, needle)
    for j, item in ipairs(haystack) do
        if item == needle then
            return j
        end
    end
    return nil
end

--- Returns a function to toggle a property of a table or object
--
-- @tparam string prop : The property to toggle
-- @treturn function : A function that toggle the prop of the input object and returns the previous value
-- @usage
-- local tog = Toggle("x")
-- local t = { x = true }
-- local prev = tog(t) -- true
-- local now = t.x -- false
-- @usage
-- awful.key(mods, "s", Toggle("sticky"), {
--      description = "toggle sticky",
--      group = "client"
-- }),
function Toggle(prop)
    return function(tbl)
        local prev = tbl[prop]
        tbl[prop] = not tbl[prop]
        return prev
    end
end

--- Define a function with local state
-- @param callback : A function that receives `state` as the first argument, `state` is a table.
-- @return A function that replies on the other non-state inputs, but has state.
function Stateful(callback, setup)
    local state = setup and setup() or {}
    state = setmetatable(state, { __mode = "kv" })
    return function(...)
        callback(state, ...)
    end
end

--- Partial application of a function.
--
-- Since ellipses can't be used here, I'll just support 5 arguments.
-- @param fn A function, or callable object.
-- @param a1 The 1st function argument.
-- @param a2 The 2nd function argument.
-- @param a3 The 3rd function argument.
-- @param a4 The 4th function argument.
-- @param a5 The 5th function argument.
-- @return A curried function that is partial application of fn.
-- @usage awful.key(mod, "Space", Partial(awful.layout.inc, 1))
function Partial(fn, a1, a2, a3, a4, a5)
    return function() return fn(a1, a2, a3, a4, a5) end
end

--- Partial application of a method
--
-- Since ellipses can't be used here, I'll just support 5 arguments.
-- @param tbl A table object.
-- @param meth The method name
-- @param a1 The 1st function argument.
-- @param a2 The 2nd function argument.
-- @param a3 The 3rd function argument.
-- @param a4 The 4th function argument.
-- @param a5 The 5th function argument.
-- @return A curried function that is partial application of tbl:meth(arg).
-- @usage
-- awful.key(mod, "w", Mpartial(menu, "show"), {
--     description = "show main menu",
--     group = "awesome"
-- })
function Mpartial(tbl, meth, a1, a2, a3, a4, a5)
    return function() return tbl[meth](tbl, a1, a2, a3, a4, a5) end
end

--- Limit a function so that it can't not execute too often
-- @param dt The minimum time delta between calls in seconds
-- @param func The function to be limited
-- @return The limited function that does everything the input function does,
-- but with limited execution rate.
function Throttle(dt, func)
    -- Create a time for this, os.clock doesn't output what we want
    -- Also destroy when reload
    local timer = lgi.GLib.Timer()
    awesome.connect_signal("exit", function()
        timer:destroy()
    end)

    -- Start counting and throttle the function
    timer:start()
    return function(...)
        local delta = timer:elapsed()
        if delta < dt then return end
        timer:reset()
        return func(...)
    end
end

--- Returns a filtered table base on some conditions
-- @tparam table table : The table to filter
-- @param fn : A predicate that receives (`value`, `key`) and output a bool
-- @treturn table : The filtered table
function Filter(table, fn)
    local out = {}
    for i, v in pairs(table) do
        if fn(v, i) then
            out[i] = v
        end
    end
    return out
end

--- Preprend a lazy argument to a function
--
-- @tparam function argfn : A lazy value (function with no arg), usually stateful
-- @tparam function fn : A function to be called
-- @treturn function : The function that calls fn with `arg` inserted
-- @usage
-- local focused = function() return capi.client.focus end
-- local client_action_menu = awful.menu({
--     items = {
--         { "float",      InsertArg(focused, Toggle("floating")) },
--         { "sticky",     InsertArg(focused, Toggle("sticky")) },
--         { "ontop",      InsertArg(focused, Toggle("ontop")) },
--         { "fullscreen", InsertArg(focused, Toggle("fullscreen")) },
--     },
-- })
function InsertArg(argfn, fn)
    return function(...)
        local arg = argfn()
        return fn(arg, ...)
    end
end

--- Filesystem functions
-- @section fsh

--- Helper funtion to list all the file in a directory
-- @param d the directory to be listed
-- @return the list of sub file and directory
function ListDir(d)
    -- read directory
    local stdout = io.popen('ls -a ' .. d):read("*a")
    local items = gears.string.split(stdout, "\n")

    -- filter special directories
    items = Filter(function(name)
        return name ~= "." and name ~= ".."
    end, items)
    return items
end

--- Return the directory name of a file path
-- @tparam string path the file path
-- @treturn string the file dirname
function Dirname(path)
    -- check if the path ends with a slash
    -- if it is remove the slash
    local len = #path
    if path:sub(len, len) == "/" then
        path = path:sub(1, len - 1)
    end

    local parts = gears.string.split(path, "/")
    parts[#parts] = nil
    return table.concat(parts, "/")
end

--- Return the basename of a file path
-- @tparam string path the file path
-- @treturn string the file basename
function Basename(path)
    -- check if the path ends with a slash
    -- if it is remove the slash
    local len = #path
    if path:sub(len, len) == "/" then
        path = path:sub(1, len - 1)
    end

    local parts = gears.string.split(path, "/")
    return parts[#parts]
end

--- Split a string using separator
-- @tparam string the string to split
-- @tparam string the separator
-- @treturn table the string components
function Split(inputstr, sep)
    sep = sep or "%s"
    local t = {}
    for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
        table.insert(t, str)
    end
    return t
end

--- Resource functions
-- @section rf

--- Get resource from configuration directory
-- @tparam string path resource path relative to the configuration directory
-- @treturn string absolute path to resource
-- @usage
-- -- assets/wallpaper.png is in the configuration directory
-- local function set_wallpaper(s)
--     local wallpaper = Resource("assets/wallpaper.png")
--     gears.wallpaper.maximized(wallpaper, s, false)
-- end
function Resource(path)
    return gears.filesystem.get_configuration_dir() .. path
end

--- Get icon resource and recolor if needed
-- @tparam string path icon path relative to configuration directory
-- @tparam string|nil color hex color string, if is nil, the icon won't be recolored
-- @treturn string|cairo.ImageSurface icon path or recolored icon
-- @usage
-- -- assets/icons/bento.svg is in the configuration directory
-- theme.layout_tile = Icon("assets/icons/bento.svg", theme.fg_focus)
function Icon(path, color)
    local ic = Resource(path)
    if color then
        return gears.color.recolor_image(ic, color)
    else
        return ic
    end
end

--- Create text mark up with color
-- @usage TextColor("Hi world", "#ff0000")
-- @tparam string text The text to be formatted
-- @tparam string color The hex color string
-- @treturn string The pango markup text.
function TextColor(text, color)
    return [[<span color="]] .. color .. [[">]] .. text .. [[</span>]]
end

--- Create color from HSL
--
-- @tparam number H : The hue, range from 0 to 360
-- @tparam number S : The saturation, range from 0 to 1
-- @tparam number L : The lightness, range from 0 to 1
-- @tparam number a : Transparency, range from 0 to 1, default to 1
-- @see https://www.rapidtables.com/convert/color/hsl-to-rgb.html
-- @treturn string : Hex string for the color
function HSL(H, S, L, a)
    a = a or 1
    H = H % 360

    -- aux variables
    local C = (1 - math.abs(2 * L - 1)) * S
    local X = C * (1 - math.abs((H / 60) % 2 - 1))
    local m = L - C / 2

    -- Compute rgb value
    local r, g, b = 0, 0, 0
    if H < 60 then
        r, g, b = C, X, 0
    elseif H < 120 then
        r, g, b = X, C, 0
    elseif H < 180 then
        r, g, b = 0, C, X
    elseif H < 240 then
        r, g, b = 0, X, C
    elseif H < 300 then
        r, g, b = X, 0, C
    else
        r, g, b = C, 0, X
    end
    r, g, b = r + m, g + m, b + m

    -- Convert to hexstring
    local hex_r = string.format("%02x", math.floor(r * 255 + 0.5))
    local hex_g = string.format("%02x", math.floor(g * 255 + 0.5))
    local hex_b = string.format("%02x", math.floor(b * 255 + 0.5))
    local hex_a = string.format("%x", math.floor(a * 255 + 0.5))
    local color = "#" .. hex_r .. hex_g .. hex_b .. hex_a
    return color
end

function RGB(r, g, b)
    local rhex = string.format("%x", r)
    local ghex = string.format("%x", g)
    local bhex = string.format("%x", b)
    rhex = #rhex < 2 and "0" .. rhex or rhex
    ghex = #ghex < 2 and "0" .. ghex or ghex
    bhex = #bhex < 2 and "0" .. bhex or bhex
    return "#" .. rhex .. ghex .. bhex
end

function RGBf(r, g, b)
    r = math.floor(r * 255 + 0.5)
    g = math.floor(g * 255 + 0.5)
    b = math.floor(b * 255 + 0.5)
    return RGB(r, g, b)
end

--- Create a context for Lua's `require` to import relative module
--
-- The reason we create the context instead of importing directly is because
-- this function uses `debug.getinfo`, which might not be very performant.
-- @return the function to import relatively
-- @usage
-- -- File hierarchy:
-- -- bars/
-- --   wibar.lua
-- --   taglist.lua
-- --   wifi.lua
-- --   ...
-- -- In file wibar.lua
-- local import = Import() -- must use at top level
-- local import = Import("bars.wibar") -- the same as the previous line
-- local taglist_widget = import("taglist")
-- local wifi_widget = import("wifi")
function Import()
    local info = debug.getinfo(2, "S")
    local config_dir = gears.filesystem.get_configuration_dir()
    local source_dir = Dirname(info.source:gsub(config_dir, ""):gsub("@", ""))
    local mod = source_dir:gsub("/", ".")
    return function(name)
        return require(mod .. "." .. name)
    end
end

--- Others
-- @section misc

--- Mutliclick handler
--
-- Handle multi-mouse clicks in buttons. This function is not very useful for now, because it does
-- not work with dragging.
-- @param args multi click options
-- @param args.dt The clicking threshold in seconds, if the time delta between two clicks is less than this, the click count when up, default 0.2
-- @param args.1 The function for single click
-- @param args.2 The function for double click
function DoubleClick(args)
    local dt = args.dt or 0.2 -- seems like a sweet spot
    local timer = lgi.GLib.Timer()

    -- Local state
    local last_click, click_selection
    local reset = function()
        last_click = nil
        click_selection = args[1]
    end

    -- The callback function for clicking
    return function(x1, x2, x3) -- can't use ellipses here....
        if last_click == nil then
            reset()
            last_click = timer:elapsed()
            gears.timer.start_new(dt, function()
                click_selection(x1, x2, x3)
                reset()
                return false
            end)
        else
            local delta = timer:elapsed() - last_click
            if delta <= dt then
                click_selection = args[2] -- switch to double click
            end
        end
    end
end

--- Subscribe to a command
--
-- This function also handles awesome-quitting and restarting.
-- @param args Spawning options
-- @param args.stdout Function that is called with each line of output on stdout, e.g. stdout(line).
-- @param args.stderr Function that is called with each line of output on stderr, e.g. stderr(line).
-- @param args.output_done Function to call when no more output is produced.
-- @param args.exit Function to call when the spawned process exits. This function gets the exit reason and code as its arguments. The reason can be "exit" or "signal". For "exit", the second argument is the exit code. For "signal", the second argument is the signal causing process termination.
-- @return PID or error message
function Subscribe(args)
    local awesome = _G.awesome
    local pid_or_error = awful.spawn.with_line_callback(args.command, {
        stdout = args.stdout,
        stderr = args.stderr,
        output_done = args.output_done,
        exit = args.exit,
    })

    -- Auto kill on awesome quit or restart
    local pid = tonumber(pid_or_error)
    if pid ~= nil then
        awesome.connect_signal("exit", function()
            io.popen("kill -9 " .. tostring(pid))
        end)
    end
    return pid_or_error
end

--- AwesomeWM C-API, for easy access and to shut luacheck up.
-- Also, ignore the docs on this one, ldoc is being silly here.
-- @usage local capi = CAPI
-- @table CAPI
CAPI = {
    root = root,       -- luacheck: ignore
    tag = tag,         -- luacheck: ignore
    mouse = mouse,     -- luacheck: ignore
    awesome = awesome, -- luacheck: ignore
    client = client,   -- luacheck: ignore
    screen = screen,   -- luacheck: globals screen
}

--- Check if is awesome-git or not
function IsGitVersion()
    local is_git = CAPI.awesome.version:match("dirty")
    return is_git ~= nil
end

--- Kill PID before awesome exit
function KillAtExit(pid)
    awesome.connect_signal("exit", function()
        awful.spawn.with_shell("kill -9 " .. tostring(pid))
    end)
end

--- Easy property for widgets
--
-- This function add set_prop, get_prop and property::prop signal to a widget or widget template.
-- @tparam table widget : Widget or prototype of widget
-- @tparam string prop : The property name
-- @usage
-- local mywidget = { mt = {} }
-- WithProp(mywidth, "prop1")
function WithProp(widget, prop)
    -- Set function
    widget["set_" .. prop] = function(self, v)
        self._private[prop] = v
        self:emit_signal("property::" .. prop, self)
    end

    -- Get function
    widget["get_" .. prop] = function(self)
        return self._private[prop]
    end
end

--- A widget container.
-- @table Hako
-- @see mod:libs.hako
local import = Import(...)
Hako = import("hako")

--- Monkey patch wibox.widget function to lazily define signals
-- do
--     local base = require("wibox.widget.base")
--     local check_widget = base.check_widget
--     rawset(base, "check_widget", function(widget)
--         local signals = widget.signals or {}
--         check_widget(widget)
--         local count = 0
--         for name, sig in pairs(signals) do
--             count = count + 1
--             widget:connect_signal(name, sig)
--         end
--         return widget
--     end)
-- end
