-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")


-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")
require("ndgnuh.taskswitcher"):setup()
require("libs.globals")
local libs = require("libs")
local ic = require("libs.icecream")
local upower = require("ndgnuh.widgets.upower")
local battery_icon = require("ndgnuh.battery_icon_upower")
local smart_switch = require("ndgnuh.smart-switch")
local smart_drag = require("ndgnuh.floating-drag")
local fuzzy_raise_or_spawn = require("libs.fuzzy_raise_or_spawn")
local iconbutton = require("libs.iconbutton")
local powermenu = require("ndgnuh.powermenu_lightdm")
local icons = require("icons")
local volatile_pause = false
local state = {}

local lgi = require("lgi")
local playerctl = require("libs.playerctl")
local LinkedList = require("libs.LinkedList")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function(err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({
            preset = naughty.config.presets.critical,
            title = "Oops, an error happened!",
            text = tostring(err)
        })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
require("libs.globals")
local theme = require('ndgnuh.theme')
beautiful.init(theme)

-- This is used later as the default terminal and editor to run.
terminal = "sakura"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
local max_layout = require("lib.workflow.layout.max")
local tile_layout = require("lib.workflow.layout.tile")
local float_layout = awful.layout.suit.floating
awful.layout.layouts = { max_layout, tile_layout, float_layout }
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
    { "hotkeys",     function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
    { "manual",      terminal .. " -e man awesome" },
    { "edit config", editor_cmd .. " " .. awesome.conffile },
    { "restart",     awesome.restart },
    { "quit",        function() awesome.quit() end },
}

mymainmenu = awful.menu({
    items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
        { "open terminal", terminal }
    }
})

mylauncher = wibox.widget {
    widget = wibox.container.margin,
    margins = beautiful.wibar_spacing / 4,
    awful.widget.launcher({
        image = beautiful.os_icon,
        menu = mymainmenu
    })
}

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock("<b>%a, %d/%m/%Y %H:%M</b>")

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
    awful.button({}, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({}, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({}, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = gears.table.join(
    awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal(
                "request::activate",
                "tasklist",
                { raise = true }
            )
        end
    end),
    awful.button({}, 3, function()
        awful.menu.client_list({ theme = { width = 250 } })
    end),
    awful.button({}, 4, function()
        awful.client.focus.byidx(1)
    end),
    awful.button({}, 5, function()
        awful.client.focus.byidx(-1)
    end))

local function set_wallpaper(s)
    -- wallpaper
    local wallpaper = Resource("assets/wallpaper-16-10.jpg")
    gears.wallpaper.fit(wallpaper, s, false)
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({}, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
        awful.button({}, 1, function() awful.layout.inc(1) end),
        awful.button({}, 3, function() awful.layout.inc(-1) end),
        awful.button({}, 4, function() awful.layout.inc(1) end),
        awful.button({}, 5, function() awful.layout.inc(-1) end)))
    -- Create a taglist widget
    -- s.mytaglist = awful.widget.taglist {
    --     screen  = s,
    --     filter  = awful.widget.taglist.filter.all,
    --     buttons = taglist_buttons
    -- }
    s.mytaglist = require("ndgnuh.cairo_taglist") {
        screen = s,
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }
    s.mytasklist = require("ndgnuh.tasklist").setup {
        screen = s,
        layout = wibox.layout.flex.vertical
    }

    -- Create the wibox
    -- s.my_other_wibox = awful.wibar {
    --     position = "top",
    --     screen = s,
    --     height = beautiful.xresources.apply_dpi(4),
    -- }
    s.mywibox = awful.wibar({
        position = "top",
        screen = s,
    })

    -- wibox {
    --     screen = s,
    --     x = 10,
    --     y = 10,
    --     width = 90,
    --     height = 100,
    --     visible = true,
    --     ontop = true,
    --     widget = wibox.widget {
    --         widget = wibox.container.background,
    --         battery_icon(),
    --         bg = "#ff0000",
    --     }
    -- }

    local client_icon = awful.titlebar.widget.iconwidget({})
    local client_title = wibox.widget {
        widget = wibox.widget.textbox,
        align = "center",
    }
    local update_client_info = function(c)
        local matcher_str = ""
        if c then
            if c.matcher then
                matcher_str = "[" .. c.matcher .. "] "
            end
            client_icon:set_client(c)
            client_title:set_markup("<b>" .. matcher_str .. tostring(c.name or c.class or "Untitled window") .. "</b>")
        end
    end
    client.connect_signal("focus", update_client_info)
    client.connect_signal("property::name", update_client_info)
    client.connect_signal("property::class", update_client_info)

    local tray_popup = awful.popup {
        visible = false,
        ontop = false,
        border_color = "#00ff00",
        bg = "#1d1f21",
        width = 300,
        widget = wibox.widget {
            widget = wibox.container.background,
            bg = beautiful.bg_normal,
            forced_height = 50,
            {
                widget = wibox.widget.systray,
                forced_width = 300
            }
        }
    }

    -- Add widgets to the wibox
    local null = wibox.widget.textbox("")
    s.mywibox:setup {
        layout = wibox.layout.flex.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            spacing = beautiful.wibar_spacing or 4,
            {
                widget = iconbutton,
                icon = icons.poweroff,
                buttons = awful.button({}, 1, function() powermenu() end),
            },
            {
                widget = require("libs.iconbutton"),
                icon = Icon("assets/icons/settings.svg"),
                buttons = awful.button({}, 1, function()
                    local mo = awful.mouse.object
                    local g = mo.get_current_widget_geometry()
                    local w = mo.get_current_widget()

                    local visible = not tray_popup.visible

                    awful.placement.top_left(tray_popup)
                    tray_popup.y = beautiful.wibar_height + beautiful.wibar_border_width * 2
                    -- awful
                    -- tray_popup.x = g.x - tray_popup.width / 2 + g.width / 2
                    -- tray_popup.y = g.y + beautiful.wibar_height
                    tray_popup.visible = visible
                    tray_popup.ontop = visible
                end)
            },
            require("ndgnuh.widgets.redshift_wibar")(),
            {
                widget = iconbutton,
                icon = icons.music,
                buttons = awful.button({}, 1, function()
                    local title = playerctl.get_title() or "Untitled"
                    local artist = playerctl.get_artist() or "Unknown"
                    naughty.notify {
                        title = "Now playing",
                        text = title .. " - " .. artist,
                    }
                end),
            },
            {
                widget = iconbutton,
                icon = icons.skip_prev,
                buttons = awful.button({}, 1, playerctl.prev),
            },
            {
                widget = iconbutton,
                icon = icons.play,
                buttons = awful.button({}, 1, playerctl.play_pause),
            },
            {
                widget = iconbutton,
                icon = icons.skip_next,
                buttons = awful.button({}, 1, playerctl.next),
            },
            s.mypromptbox,
            -- s.mytasklist,
        },
        client_title,
        { -- Right widgets
            widget = wibox.container.place,
            halign = "right",
            {
                layout = wibox.layout.fixed.horizontal,
                spacing = beautiful.wibar_border_width,
                require("ndgnuh.ibus-spawn")(),
                battery_icon(),
                mytextclock,
                -- s.mylayoutbox,
            }
        },
    }

    -- Add widgets to the taglist wibox
    -- s.my_other_wibox:setup {
    --     layout = wibox.layout.flex.vertical,
    --     s.mytaglist,
    -- }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({}, 3, function() mymainmenu:toggle() end),
    awful.button({}, 4, awful.tag.viewnext),
    awful.button({}, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey }, "e", function() awful.spawn("ibus engine xkb:us::eng") end),
    awful.key({ modkey }, "v", function() awful.spawn("ibus engine Bamboo") end),
    awful.key({ modkey }, "a", function() awful.spawn("ibus engine anthy") end),
    awful.key({ modkey, }, "s", hotkeys_popup.show_help,
        { description = "show help", group = "awesome" }),
    awful.key({ modkey, }, "Left", awful.tag.viewprev,
        { description = "view previous", group = "tag" }),
    awful.key({ modkey, }, "Right", awful.tag.viewnext,
        { description = "view next", group = "tag" }),
    awful.key({ modkey, }, "Escape", awful.tag.history.restore,
        { description = "go back", group = "tag" }),
    -- awful.key({ modkey, }, "j", function()
    --     local require("attic.taskswitcher")
    -- end, { description = "focus next by index", group = "client" }),
    -- awful.key({ modkey, }, "k", awful.tag.viewprev, { description = "focus previous by index", group = "client" }),
    awful.key({ modkey, }, "0", awful.tag.viewnone, { description = "show desktop", group = "client" }),
    awful.key({ modkey, }, "w", function() mymainmenu:show() end,
        { description = "show main menu", group = "awesome" }),

    -- Layout manipulation
    awful.key({ modkey, "Shift" }, "j", function() awful.client.swap.byidx(1) end,
        { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, "Shift" }, "k", function() awful.client.swap.byidx(-1) end,
        { description = "swap with previous client by index", group = "client" }),
    awful.key({ modkey, "Control" }, "j", function() awful.screen.focus_relative(1) end,
        { description = "focus the next screen", group = "screen" }),
    awful.key({ modkey, "Control" }, "k", function() awful.screen.focus_relative(-1) end,
        { description = "focus the previous screen", group = "screen" }),
    awful.key({ modkey, }, "u", awful.client.urgent.jumpto,
        { description = "jump to urgent client", group = "client" }),
    awful.key({ modkey, }, "Tab", smart_switch,
        { description = "go back", group = "client" }),

    -- Standard program
    awful.key({ modkey, }, "Return", function() awful.spawn(terminal) end,
        { description = "open a terminal", group = "launcher" }),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
        { description = "reload awesome", group = "awesome" }),
    awful.key({ modkey, "Shift" }, "q", powermenu,
        { description = "quit awesome", group = "awesome" }),
    awful.key({ modkey, }, "l", function() awful.tag.viewnext() end,
        { description = "increase master width factor", group = "layout" }),
    awful.key({ modkey, }, "h", function() awful.tag.viewprev() end,
        { description = "decrease master width factor", group = "layout" }),
    awful.key({ modkey, "Shift" }, "h", function()
            local c = client.focus
            local t = c.first_tag
            local s = c.screen
            local tags = s.tags
            local i = t.index - 1
            if i <= 0 then
                i = #tags
            elseif i > #tags then
                i = 1
            end
            c:move_to_tag(tags[i])
            tags[i]:view_only()
        end,
        { description = "increase the number of master clients", group = "layout" }),
    awful.key({ modkey, "Shift" }, "l", function()
            local c = client.focus
            local t = c.first_tag
            local s = c.screen
            local tags = s.tags
            local i = t.index - 1
            if i <= 0 then
                i = #tags
            elseif i > #tags then
                i = 1
            end
            c:move_to_tag(tags[i])
            tags[i]:view_only()
        end,
        { description = "decrease the number of master clients", group = "layout" }),
    awful.key({ modkey, "Control" }, "h", function() awful.tag.incncol(1, nil, true) end,
        { description = "increase the number of columns", group = "layout" }),
    awful.key({ modkey, "Control" }, "l", function() awful.tag.incncol(-1, nil, true) end,
        { description = "decrease the number of columns", group = "layout" }),

    -- layouts
    awful.key({ modkey, }, "t", function()
            volatile_pause = true
            naughty.notify { text = "Next client will be on the same tag!" }
        end,
        { description = "Pause volatile-zation", group = "layout" }),
    -- awful.key({ modkey, }, "t", function() awful.layout.set(tile_layout) end,
    --     { description = "tile layout", group = "layout" }),
    -- awful.key({ modkey, }, "m", function() awful.layout.set(max_layout) end,
    --     { description = "max layout", group = "layout" }),
    -- awful.key({ modkey, }, "f", function() awful.layout.set(float_layout) end,
    --     { description = "float layout", group = "layout" }),
    awful.key({ modkey, }, "space", function() awful.layout.inc(1) end,
        { description = "select next", group = "layout" }),
    awful.key({ modkey, "Shift" }, "space", function() awful.layout.inc(-1) end,
        { description = "select previous", group = "layout" }),

    awful.key({ modkey, "Control" }, "n",
        function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal(
                    "request::activate", "key.unminimize", { raise = true }
                )
            end
        end,
        { description = "restore minimized", group = "client" }),

    -- Prompt
    awful.key({ modkey, }, "r", function() require("ndgnuh.runprompt").run() end,
        { description = "Prompt", group = "awesome" }),

    -- Media keys
    awful.key({}, "XF86AudioMute", function() awful.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle", false) end,
        { description = "Toggle mute", group = "awesome" }),
    awful.key({}, "XF86AudioLowerVolume",
        function() awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%", false) end,
        { description = "Lower volume", group = "awesome" }),
    awful.key({}, "XF86AudioRaiseVolume",
        function() awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%", false) end,
        { description = "Raise volume", group = "awesome" }),
    awful.key({}, "XF86AudioMicMute", function() awful.spawn("pactl set-source-mute @DEFAULT_SOURCE@ toggle", false) end,
        { description = "Toggle microphone mute", group = "awesome" }),
    awful.key({}, "XF86MonBrightnessDown", function() awful.spawn("brightnessctl s 10%-", false) end,
        { description = "Decrease brightness", group = "awesome" }),
    awful.key({}, "XF86MonBrightnessUp", function() awful.spawn("brightnessctl s 10%+", false) end,
        { description = "Increase brightness", group = "awesome" }),
    awful.key({}, "XF86Display", function() awful.spawn("arandr") end,
        { description = "Take a screenshot", group = "awesome" }),
    awful.key({}, "Print", function() awful.spawn("spectacle") end,
        { description = "Take a screenshot", group = "awesome" }),
    awful.key({ modkey }, "F9", function() awful.spawn("spectacle") end,
        { description = "Take a screenshot", group = "awesome" }),
    awful.key({ modkey, "Shift", }, "F9", function() awful.spawn("spectacle --region") end,
        { description = "Take a screenshot (region)", group = "awesome" }),
    awful.key({ modkey, "Control", }, "F9", function() awful.spawn("spectacle -u") end,
        { description = "Take a screenshot (window under cursor)", group = "awesome" }),
    awful.key({ modkey }, "Print", function() awful.spawn.with_shell("spectacle --region") end,
        { description = "Take a screenshot", group = "awesome" }),
    awful.key({ modkey }, "Print", function() awful.spawn.with_shell("spectacle --region") end,
        { description = "Take a screenshot", group = "awesome" }),

    -- quick launch
    awful.key({ modkey }, 1, function() fuzzy_raise_or_spawn { command = "sakura" } end,
        { description = "Run thunar (file manager)", group = "Applications" }),
    awful.key({ modkey }, 2, function() fuzzy_raise_or_spawn { command = "firefox-devedition", pattern = "firefox" } end,
        { description = "Run browser (firefox)", group = "Applications" }),
    awful.key({ modkey }, 3, function() fuzzy_raise_or_spawn { command = "sakura", pattern = "terminal-2" } end,
        { description = "Run sakura", group = "Applications" }),
    awful.key({ modkey }, 4, function() fuzzy_raise_or_spawn { command = "sakura", pattern = "terminal-3" } end,
        { description = "Run sakura", group = "Applications" }),
    awful.key({ modkey }, 5,
        function()
            fuzzy_raise_or_spawn {
                command = "onlyoffice-desktopeditors",
                pattern = "OnlyOffice Desktop Editors"
            }
        end,
        { description = "Run onlyoffice", group = "Applications" }),
    awful.key({ modkey }, 7, function() fuzzy_raise_or_spawn { command = "lyx" } end,
        { description = "LyX", group = "Applications" }),
    awful.key({ modkey }, 8, function() fuzzy_raise_or_spawn { command = "qpdfview" } end,
        { description = "TeX Studio", group = "Applications" }),
    awful.key({ modkey }, 9, function() fuzzy_raise_or_spawn { command = "thunar" } end,
        { description = "Thunar", group = "Applications" }),

    awful.key({ modkey, "Shift" }, "m", function()
        _G.marked_client = client.focus
    end, { description = "Mark this client", group = "Applications" }),
    awful.key({ modkey }, "m", function()
        local c = _G.marked_client
        if c then
            c:emit_signal("request::activate", "fuzzy_matched", { raise = true })
            c.first_tag:view_only()
            client.focus = c
            c:raise()
        end
    end, { description = "Focus marked client", group = "Applications" }),

    awful.key({ modkey }, "x",
        function()
            awful.prompt.run {
                prompt       = "Run Lua code: ",
                textbox      = awful.screen.focused().mypromptbox.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir() .. "/history_eval"
            }
        end,
        { description = "lua execute prompt", group = "awesome" }),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
        { description = "show the menubar", group = "launcher" })
)

clientkeys = gears.table.join(
    awful.key({ modkey, }, "f", function(c) c.floating = not c.floating end,
        { description = "toggle floating", group = "client" }),
    awful.key({ modkey, "Shift" }, "f",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        { description = "toggle fullscreen", group = "client" }),
    awful.key({ modkey, "Shift" }, "c", function(c) c:kill() end,
        { description = "close", group = "client" }),
    awful.key({ modkey, "Control" }, "space", awful.client.floating.toggle,
        { description = "toggle floating", group = "client" }),
    awful.key({ modkey, "Control" }, "Return", function(c) c:swap(awful.client.getmaster()) end,
        { description = "move to master", group = "client" }),
    awful.key({ modkey, }, "o", function(c) c:move_to_screen() end,
        { description = "move to screen", group = "client" }),
    awful.key({ modkey, "Shift" }, "t", function(c) c.ontop = not c.ontop end,
        { description = "toggle keep on top", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
-- for i = 1, 9 do
--     globalkeys = gears.table.join(globalkeys,
--         -- View tag only.
--         awful.key({ modkey }, "#" .. i + 9,
--             function()
--                 local screen = awful.screen.focused()
--                 local tag = screen.tags[i]
--                 if tag then
--                     tag:view_only()
--                 end
--             end,
--             { description = "view tag #" .. i, group = "tag" }),
--         -- Toggle tag display.
--         awful.key({ modkey, "Control" }, "#" .. i + 9,
--             function()
--                 local screen = awful.screen.focused()
--                 local tag = screen.tags[i]
--                 if tag then
--                     awful.tag.viewtoggle(tag)
--                 end
--             end,
--             { description = "toggle tag #" .. i, group = "tag" }),
--         -- Move client to tag.
--         awful.key({ modkey, "Shift" }, "#" .. i + 9,
--             function()
--                 if client.focus then
--                     local tag = client.focus.screen.tags[i]
--                     if tag then
--                         client.focus:move_to_tag(tag)
--                     end
--                 end
--             end,
--             { description = "move focused client to tag #" .. i, group = "tag" }),
--         -- Toggle tag on focused client.
--         awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
--             function()
--                 if client.focus then
--                     local tag = client.focus.screen.tags[i]
--                     if tag then
--                         client.focus:toggle_tag(tag)
--                     end
--                 end
--             end,
--             { description = "toggle focused client on tag #" .. i, group = "tag" })
--     )
-- end

local clientbuttons
clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey, "Control" }, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        libs.client.normalize(c)
    end),
    awful.button({ modkey }, 1, function(c)
        awful.mouse.client.move(c, false) -- disable snap
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey }, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
local keypass_first_instance = true
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen
        },
        callback = function(c)
            if c.class == "qpdfview" then
                -- normalize
                c.fullscreen           = false
                c.maximized            = false
                c.maximized_vertical   = false
                c.maximized_horizontal = false
            end
        end,
    },

    -- Ignore these clients
    {
        rule_any = {
            role = {
                "WebRTCGlobalIndicator"
            },
        },
        properties = {
            minimized = true,
            focus = false,
        },
    },

    {
        rule_any = {
            instance = {
                "xfce4-terminal",
                "sakura",
            },
        },
        properties = {
            size_hints_honor = false,
        }
    },

    -- Floating + Sticky
    {
        rule_any = {
            instance = {
                "dragon",
                "vlc",
                "gscreenshot",
                "spectacle",
                "arandr",
            },
            name = {
                "picture-in-picture",
                "Picture-in-Picture",
            },
        },
        properties = {
            floating = true,
            ontop = true,
            sticky = true,
            border_color = "#0000ff",
            titlebars_enabled = true,
            is_dialog = true,
        },
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {
                "DTA",   -- Firefox addon DownThemAll.
                "copyq", -- Includes session name in class.
                "pinentry",
            },
            class = {
                "dragon",
                "Arandr",
                "Blueman-manager",
                "Gpick",
                "Kruler",
                "MessageWin",  -- kalarm.
                "Sxiv",
                "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                "Wpa_gui",
                "veromix",
                "xtightvncviewer" },

            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name = {
                "Event Tester", -- xev.
            },
            role = {
                "AlarmWindow",   -- Thunderbird's calendar.
                "ConfigManager", -- Thunderbird's about:config.
                "pop-up",        -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = {
            floating = true,
            ontop = true,
            border_color = "#0000ff",
            titlebars_enabled = true,
            is_dialog = true,
        }
    },

    -- spawning normal window will create a volatile tag
    -- this rule has to be placed below the above rule,
    -- so that we can rule out the floating windows
    -- TODO: find out why qpdfview does not match this
    {
        rule_any = {
            type = { "normal" },
        },
        properties = {
            titlebars_enabled = false,
        },
        callback = function(c)
            -- ignore floating windows
            if c.floating then
                return
            end

            -- normalize
            libs.client.normalize(c)

            -- temporary pause
            if volatile_pause then
                volatile_pause = false
                naughty.notify { text = "next application will be moved to volatile tag" }
                return
            else
                libs.client.move_to_volatile(c, tile_layout)
            end
        end
    },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("property::floating", function(c)
    if c.floating then
        c.titlebars_enabled = true
        c.border_color = "#0000ff"
    else
        c.titlebars_enabled = false
    end
end)

client.connect_signal("manage", function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
        and not c.size_hints.user_position
        and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({}, 1, function()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            smart_drag(c)
        end),
        awful.button({}, 3, function()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.resize(c)
        end)
    )

    c.titlebar = awful.titlebar(c)
    c.titlebar:setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        {     -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton(c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton(c),
            awful.titlebar.widget.ontopbutton(c),
            awful.titlebar.widget.closebutton(c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

-- client.connect_signal("property::minimized", function(c)
--     if c.is_dialog then
--         naughty.notify { message = ("can't minimize a dialog (for now)") }
--         awful.client.restore()
--     end
-- end)
client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- Draft
-- fixed size list view
local get_hl_index = function(i, limit)
    local item = i % limit
    item = item == 0 and limit or item
    local page = math.ceil(i / limit)
    return item, page
end

local list_view = function(args)
    local widget_template = args.widget_template
    local data = args.data or {}
    local n = args.size or n
    local orientation = args.orientation or "vertical"
    local bg_active = args.bg_active or beautiful.bg_focus
    local bg_normal = args.bg_normal or beautiful.bg_normal

    local layout = wibox.layout.flex[orientation]()
    layout.current = 1
    layout.data = data

    function layout:rerender()
        local children = self.children
        local i, p = get_hl_index(layout.current, n)
        for j, c in ipairs(children) do
            c.bg = i == j and bg_active or bg_normal
        end
    end

    function layout:select(i)
        self.current = i
        self:rerender()
    end

    function layout:select_next()
        self.current = math.min(self.current + 1, #self.data)
        self:rerender()
    end

    function layout:select_prev()
        self.current = math.max(self.current - 1, 1)
        self:rerender()
    end

    for i = 1, n do
        local w = wibox.widget {
            widget = wibox.container.background,
            bg = i == current and bg_active or bg_normal,
            id = "background_role",
            widget_template
        }
        w:connect_signal("button::press", function()
            layout:select(i)
        end)
        layout:add(w)
    end

    return layout
end

local function mock()
    local lv = list_view {
        size = 3,
        data = { 1, 2, 3, 4, 5, 6, 7 },
        widget_template = {
            widget = wibox.widget.textbox,
            text = "fill me",
            id = 'text_role',
        }
    }

    local p = wibox.widget.textbox("prev")
    p:connect_signal("button::press", function() lv:select_prev() end)
    local n = wibox.widget.textbox("next")
    n:connect_signal("button::press", function() lv:select_next() end)
    return wibox.widget {
        widget = wibox.layout.align.horizontal,
        p, lv, n,
    }
end


wibox {
    width = 224,
    height = 50,
    visible = false,
    ontop = false,
    widget = wibox.widget {
        shape = gears.shape.circle,
        widget = require("libs.iconbutton"),
        icon = Icon("assets/icons/settings.svg"),
    },
}

awful.spawn.single_instance("dex -a -s ~/.config/autostart && read")
