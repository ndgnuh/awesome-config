--[[-- A wrapper around survival lib to make user settings

@usage -- Setup and retrieve value
local settings = require("libs.settings")
settings.default("terminal", "xterm") -- Set default terminal
local terminal = settings.get("terminal") -- Might not be "xterm"

@usage -- Widgets
local settings = require("libs.settings")
settings.show_settings_panel()

@usage -- Widgets but it should have been this
local settings = require("libs.settings")
settings.show {
    {
        key = "terminal",
        title = "Terminal command",
        type = "text",
    },
    {
        key = "modkey",
        type = "choice",
        choices = { "Mod1", "Mod4" }
    },
    {
        key = "num_tags",
        title = "Number of tags",
        type = "number",
        default = 5,
    }
}
]]
local import = Import()
local survival = require("libs.survival")
local ctx = survival("settings")
-- local settings_widgets = import("widgets")

local settings = {
    _private = {},
    mt = {},
}

--- Toggle the setting panels
settings.show_settings_panel = function() settings_widgets(settings) end

--- Save current settings.
settings.save = function()
    ctx.serialize(settings._private)
end

--- Load settings from disks.
settings.load = function()
    settings._private = ctx.deserialize() or {}
end

--- Get config value by key.
--
-- @tparam string key : The configuration key
-- @return : Configuration value
function settings.get(key)
    return rawget(settings._private, key)
end

--- Set config and save it.
--
-- @param key string: The configuration key
-- @param value any: The configuration object
function settings.set(key, value)
    rawset(settings._private, key, value)
    settings.save()
end

--- Get config value with default.
--
-- If the configured value is nil, the default will be saved.
-- @tparam string key : The config key
-- @param default : The default config value
-- @return : Default value if the key is not set, else the configured value
settings.default = function(key, default)
    local value = settings[key]
    if value == nil then
        settings[key] = default
        settings.save()
        return default
    else
        return value
    end
end

return setmetatable(settings, settings.mt)
