-- This module is to reduce "magic" values
local surface = require("gears.surface")
local ic = require("libs.icecream")
local icons = {}

icons.settings = Icon("assets/icons/settings.svg")
icons.poweroff = Icon("assets/icons/power_settings_circle_96dp_E8EAED_FILL1_wght400_GRAD0_opsz48.svg")
icons.reboot = Icon("assets/icons/restart_alt_96dp_E8EAED_FILL1_wght400_GRAD0_opsz48.svg")
icons.logout = Icon("assets/icons/logout_96dp_E8EAED_FILL1_wght400_GRAD0_opsz48.svg")
icons.redshift = Icon("assets/icons/wb_incandescent.svg")
icons.arrow_forward = Icon("assets/icons/arrow_forward_96dp_E8EAED_FILL1_wght400_GRAD0_opsz48.svg")
icons.skip_next = Icon("assets/icons/skip_next_96dp_E8EAED_FILL1_wght400_GRAD0_opsz48.svg")
icons.skip_prev = Icon("assets/icons/skip_previous_96dp_E8EAED_FILL1_wght400_GRAD0_opsz48.svg")
icons.play = Icon("assets/icons/play_arrow_96dp_E8EAED_FILL1_wght400_GRAD0_opsz48.svg")
icons.pause = Icon("assets/icons/pause_96dp_E8EAED_FILL1_wght400_GRAD0_opsz48.svg")
icons.music = Icon("assets/icons/music_note_96dp_E8EAED_FILL1_wght400_GRAD0_opsz48.svg")

-- aliases
icons.shutdown = icons.poweroff
icons.restart = icons.reboot

return icons
