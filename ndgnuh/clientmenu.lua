--[[-- Client menu widgetc

@widget ndgnuh.clientmenu

]]
local clientmenu = {}
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local gears = require("gears")
local widgets = require("ndgnuh.widgets")

--- Client property widget
local client_prop = { mt = {} }

function client_prop:my_update()
    local c = self:get_client()
    local prop = self:get_prop()
    local label = self:get_label() or prop or ""
    if not c or not prop then return end
    local markup
    if c and c[prop] then
        markup = TextColor(label, self.fg_active or beautiful.fg_focus)
    else
        markup = TextColor(label, self.fg_normal or beautiful.fg_normal)
    end
    self:set_markup(markup)
end

WithProp(client_prop, "label")
WithProp(client_prop, "client")
WithProp(client_prop, "prop")
WithProp(client_prop, "fg_active")
WithProp(client_prop, "fg_normal")

function client_prop.mt:__call()
    local proxy = wibox.widget.textbox()
    local widget = wibox.widget.base.make_widget(nil, "widget.client.property", {
        enable_properties = true,
        class = client_prop,
    })
    gears.table.crush(widget, proxy, false)
    gears.table.crush(widget, client_prop, false)

    widget:connect_signal("property::label", widget.my_update)
    widget:connect_signal("property::client", widget.my_update)
    widget:connect_signal("property::prop", widget.my_update)

    widget:buttons(awful.button({}, 1, function()
        local c = widget.client
        local prop = widget.prop
        c[prop] = not c[prop]
        widget:myupdate()
    end))

    return widget
end

client_prop = setmetatable(client_prop, client_prop.mt)

--- Client properties
--
-- The table items are tables.
-- Each items is <property> <label>
-- @table properties
clientmenu.properties = {
    { "sticky",    "Sticky" },
    { "floating",  "Floating" },
    { "ontop",     "Ontop" },
    { "ontop",     "Fullscreen" },
    { "maximized", "Maximized" },
    { "minimized", "Minimize" },
}

--- Returns theme variable used by client menu
--

--- Build client menu widget
--
-- @param c : AwesomeWM client.
clientmenu.build_widget = function(c)
    return wibox.widget {
        widget = widgets.client_prop,
        prop = "ontop",
        client = c,
    }

    -- local layout = wibox.layout.fixed.vertical()
    -- for _, prop in ipairs(clientmenu.properties) do
    --     local key = prop[1]
    --
    --     -- markup
    --     local widget = wibox.widget {
    --         widget = widgets.client_prop,
    --         prop = prop[1],
    --         label = prop[2],
    --         client = c,
    --         forced_height = 20,
    --         forced_width = 100,
    --     }
    --     layout:add(widget)
    -- end
    -- return layout
end

--- Show client menu, bind with some client.
--
-- @param c : AwesomeWM client.
local popup
clientmenu.show = Stateful(function(state, c)
    -- Caching and toggling
    state.showing = state.showing or setmetatable({}, { __mode = "kv" })
    state.popups = state.popups or setmetatable({}, { __mode = "kv" })

    if popup == nil then
        popup = awful.popup {
            widget = clientmenu.build_widget(c),
            border_width = 3,
            border_color = "#ff0000",
            bg_color = "#00ff00",
        }
    end
    popup.widget:set_client(c)

    awful.placement.under_mouse(popup)
    popup.visbile = true
    popup.ontop = popup.visbile
    -- pop.visbile = not pop.visbile
    -- pop.ontop = pop.visbile
end)

return clientmenu
