local awful = require("awful")
if not IsGitVersion() then
    awful.keyboard = require("monkey_patch.awful_keyboard")
end
