local awful = require("awful")
local wibox = require("wibox")
local Set = require("libs.sets")
local capi = CAPI
local settings = require("libs.settings")
settings.load()

local modkey = settings.modkey

local harpoon = {
    showing = false,
    popup = nil,
    marked = Set.new(function(c) return c.window end),
}

function harpoon:mark_client(c)
    harpoon.mark_client:push(c)
end

function harpoon:picking()
    if harpoon.popup == nil then
        return false
    end
    return harpoon.popup.visible
end

--- Helper for adding doc to keybinding
-- @param help : the description message of keybinding
local function doc(help)
    return {
        group = "Harpoon",
        description = help
    }
end

-- Add global keybinding from the module
-- =====================================
awful.keyboard.append_global_keybindings {
    awful.key({ modkey }, "u", function()
        local c = CAPI.client.focus
        harpoon.marked:add(c)
    end, doc("Toggle client mark")),

    -- show marked clients
    awful.key({ modkey }, "y", function()
        local client_widgets = {}
        ic(harpoon.marked)
        for i, c in harpoon.marked:iter() do
            table.insert(client_widgets, {
                widget = wibox.widget.textbox,
                text = tostring('') .. ". " .. c.name,
            })
        end

        if harpoon.popup == nil then
            harpoon.popup = awful.popup {
                ontop = true,
                placement = awful.placement.centered,
                widget = wibox.widget {
                    widget = wibox.layout.fixed.vertical,
                    table.unpack(client_widgets),
                }
            }
        end

        -- Toggle popup
        harpoon.popup.visible = not harpoon.popup.visible

        -- Toggle keygrabber
        if harpoon:picking() then
        end
    end, doc("Harpoon")),
}
