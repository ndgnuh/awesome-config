local W = require("wibox.widget")
local C = require("wibox.container")
local L = require("wibox.layout")
local M = {}

local button_config = function(args)
end

--- Create a new button declaration
-- This is the basic function to create a high-level button. The argument is passed in as a table.
-- If the argument is not provided, the default from beautiful will be used.
-- @param text The button texts string
-- @param icon The button icon path string or widget, optional
-- @param on_click The click callback
-- @param on_right_click The right click callback
-- @param on_middle_click The middle click callback
-- @param bg_normal The normal background color
-- @param bg_hover The background color when hover over the button
-- @param bg_click The background color when clicked
-- @param fg_normal The text color when clicked
-- @param fg_hover The text color when hover over the button
-- @param fg_click The text color when clicked
-- @param id The id for the button, sub-component will be id accordingly
-- @param text_align The horizontal text alignment
-- @param text_vertical_align The vertical text alignment
-- @return A declarative widget table (not a wibox.widget)
-- @see button_config
local new = function(args)
	args = button_config(args)
	return {
		widget = W.textbox,
		text = args.text,
	}
end


return setmetatable(M, { __call = function(self, ...) return new(...) end })
