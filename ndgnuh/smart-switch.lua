local awful = require("awful")
local LinkedList = require("libs.LinkedList")
local ic = require("libs.icecream")
local gears = require("gears")

-- create a linked list of clients
-- connect and manage signals so that the focused client
-- is always ontop, basically a stack
local clist = LinkedList {
    -- data_hash = function(c) return c.window end
}

--- For debugging only
function clist:dump()
    local args = {}
    self:foreach(function(c)
        table.insert(args, tostring(c))
    end)
    ic(args)
end

-- local data = "Data 1"
-- local data2 = "Data 2"
-- local data3 = "Data 3"
-- local data4 = "Data 4"
-- clist:dump()
-- clist:append(data)
-- clist:append(data2)
-- clist:append(data3)
-- clist:append(data4)
-- clist:dump()
-- local n1 = clist:find(data2)
-- local n2 = clist:last_node()
-- clist:attach_at(n1, n2)
-- clist:dump()
-- local n3 = clist:first_node()
-- ic(n3.data)
-- clist:remove(n3)
-- clist:dump()


-- when a client destroyed
-- client.connect_signal("unmanage", function(c)
--     ic("Removing " .. tostring(c))
--     clist:dump()
--     local node = clist:find(c)
--     clist:remove(node)
--     clist:dump()
-- end)

-- client created
-- local adding = {}
-- client.connect_signal("manage", function(c)
--     adding[c] = true
--     local new_node = clist:find(c)
--     if not new_node then
--         new_node = clist:append(c)
--     end
--     adding[c] = false
-- end)

--- Here c is the unfocused client
local previous
client.connect_signal("unfocus", function(c)
    gears.timer.delayed_call(function()
        if c.valid then
            previous = c
        else
            clist:remove(clist:find(c))
        end
    end)
end)

client.connect_signal("focus", function(c)
    gears.timer.delayed_call(function()
        local prev_node
        local this_node = clist:find(c)

        -- find previous client
        if previous and previous.valid then
            prev_node = clist:find(previous)
        end

        -- fallback to last in list
        if not prev_node then
            prev_node = clist:last_node()
        end

        -- append to last
        if this_node then
            clist:attach_at(this_node, prev_node)
        else
            clist:append(c)
        end
    end)
end)

-- when a client created or focused
client.connect_signal("f_ocus", function(c)
    local s = awful.screen.focused()
    local n = #s.clients
    -- find the previously focused client
    local last_node = clist:last_node()
    local this_node = clist:find(c)

    -- when a client created
    -- this client is new
    if this_node == nil then
        this_node = clist.Node(c)
    end

    -- account for the fact that sometime
    -- unfocused client are invalid (when closed)
    for _ = 1, n do
        if last_node == clist.first then
            break
        end

        if not last_node.data.valid then
            last_node = last_node.prev
        end
    end

    -- pop this node and move to the top
    clist:attach_at(this_node, last_node)
end)

local switch_old = function(state)
    -- smart switch back behaviour
    if state.tag_mode and state.prev_tag then
        local s = awful.screen.focused()
        local cur_tag = s.selected_tag
        local prev_tag = state.prev_tag
        prev_tag:view_only()
        state.prev_tag = cur_tag
    else
        -- default is just focus.history.previous
        awful.client.focus.history.previous()
    end

    -- raise
    local c = client.focus
    if c then
        c.minimized = true
        c.minimized = false
        c:raise()
    end
end

local function switch()
    -- get next to last node
    -- which is the last focused client
    local cnode = clist:last_node().prev

    -- empty list
    if cnode == clist.first then return end

    local c = cnode.data
    if c and c.valid then
        c.first_tag:view_only()
        c.minimized = false
        client.focus = c
        c:raise()
    end
end

local function init() --setup function
    -- local state = { tag_mode = false, prev_tag = nil }
    --
    -- -- connect to unfocus signal to switch between modes
    -- -- if it is a tag switch, the current tag and the unfocused
    -- -- tag will be different
    -- client.connect_signal("unfocus", function(c)
    --     local s = awful.screen.focused()
    --     local c_tag = c.first_tag
    --     local cur_tag = s.selected_tag
    --     if c_tag ~= cur_tag then
    --         state.prev_tag = c_tag
    --         state.tag_mode = true
    --     else
    --         state.tag_mode = false
    --     end
    -- end)
    --
    -- -- return initialized state
    -- return state
end

return setmetatable({ state = init() }, { __call = function(self) switch(self.state) end })
