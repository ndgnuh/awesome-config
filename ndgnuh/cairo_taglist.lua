local taglist = { mt = {} }
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local awful = require("awful")

local ic = require("libs.icecream")

function taglist:get_information()
    local s = self:get_screen()
    return {
        screen = s,
        tags = s.tags,
        selected_tags = s.selected_tags
    }
end

function taglist:get_theme()
    local theme = {}
    theme.color_active = beautiful.taglist_bg_focus or "#fff"
    theme.color_normal = beautiful.taglist_bg_normal or beautiful.wibar_bg or beautiful.bg_normal or "#000"
    return theme
end

function taglist:set_screen(s)
    self._private.screen = s
    self:emit_signal("property::screen")
end

function taglist:set_vertical(flag)
    self._private.vertical = flag
    self:emit_signal("property::vertical")
    self:emit_signal("widget::redraw_needed")
end

function taglist:get_screen()
    return self._private.screen
end

function taglist:get_vertical()
    return self._private.vertical
end

function taglist:fit(ctx, w, h)
    local ret = wibox.layout.stack.fit(self, ctx, w, h)
    return ret
end

function taglist:draw(ctx, cr, w, h)
    local s = self:get_screen()
    local vertical = self:get_vertical()
    local theme = self:get_theme()
    local tags = s.tags
    local n = #tags

    vertical = (vertical == nil) and (w < h) or vertical
    for i, tag in ipairs(tags) do
        local x, y, wi, hi
        if vertical then
            x = 0
            y = (i - 1) * h / n
            hi = h / n
            wi = w
        else
            x = (i - 1) * w / n
            y = 0
            hi = h
            wi = w / n
        end
        cr:rectangle(x, y, wi, hi)
        if tag.selected then
            cr:set_source(gears.color(theme.color_active))
        else
            cr:set_source(gears.color(theme.color_normal))
        end
        cr:fill()
        cr:close_path()
    end
end

local function new(args)
    local widget = wibox.widget.base.make_widget()
    gears.table.crush(widget, taglist, true)
    widget:set_screen(args.screen or awful.screen.focused())
    widget:set_vertical(args.vertical or nil)

    screen.connect_signal("tag::history::update", function()
        widget:emit_signal("widget::redraw_needed")
    end)
    return widget
end


return setmetatable(taglist, { __call = function(_, args) return new(args) end })
