--[[-- Custom global signals.

Volume, network, brightness, power, etc.

@usage
require("libs.signals") -- require to set it up

awesome.connect_singal("signal::name", function(params)
    -- Do something with callback params
end)

-- Example for setting up a wibox for volume
awesome.connect_singal("volume::changed", function(args)
    textbox:set_markup("Volume: " .. tostring(args.volume))
end)

--]]
local awful = require("awful")
local signals = {}
local capi = CAPI

--- Emit when volume status is changed
--
-- @signal volume::changed
-- @number volume The volume percentage
-- @bool muted True iff muted
local setup = function()
    Subscribe {
        command = "pactl subscribe",
        stdout = Throttle(0.2, function(_) -- The subscribe does not provide anything useful
            -- callback for getting volume
            awful.spawn.easy_async_with_shell("pactl get-sink-volume @DEFAULT_SINK@", function(volume_out, err1)
                -- callback for getting muted state
                awful.spawn.easy_async_with_shell("pactl get-sink-mute @DEFAULT_SINK@", function(mute_out, err2)
                    -- show error if any
                    assert(err1 == "", err1)
                    assert(err2 == "", err2)

                    -- emit signals
                    local volume = volume_out:match("%d+%%")
                    local mute = mute_out:match("yes") ~= nil
                    local params = setmetatable({ volume = volume, mute = mute }, { __mode = "kv" })
                    capi.awesome.emit_signal("volume::changed", params)
                end)
            end)
        end),
        stderr = ic,
        exit = ic,
    }
end

return { setup = setup }
