--- A WIP widget to indicate tagnames
--
-- Intended features: tag rename
local textbox = require("wibox.widget.textbox")
local awful = require("awful")
local gtimer = require("gears.timer")
local capi = CAPI
local tagname = { mt = {} }

--- Create a tagname widgets
function tagname.mt:__call() -- luacheck: no unused
    local widget = textbox()
    capi.screen.connect_signal("tag::history::update", function()
        -- Delay because sometime the tag gets deleted
        -- This won't be noticable
        gtimer.start_new(0.025, function()
            widget:set_markup(awful.screen.focused().selected_tag.name)
            return false
        end)
    end)
    return widget
end

return setmetatable(tagname, tagname.mt)
