--[[-- Action menu widget.

Workflow
---------

1. Press Mod + P,
2. A bunch of stuffs shows up, allowing keybindings,
3. Press anykey to disable the popup, press keybinding to perform actions,
4. Perform action if applicable
5. Used to perform minor tasks, such as rename client, show/hide system tray, calendar, etc.

Intended usage
---------------

@usage
local ActionMenu = require("ndgnuh.acitonmenu")

action_menu = ActionMenu{
    actions = {
        ["m"] = function() ... end,
        ["t"] = function() ... end,
    }
}

awful.key({ modkey }, "p", function()
    action_menu:show()
end)
--]]
local actionmenu = {}

function actionmenu:create_popup()
end

return actionmenu
