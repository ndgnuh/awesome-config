local gears = require('gears')
local workflow = {}

--- Setup signal handling and related features.
--
-- @usage require("libs.workflow.tags").setup()
workflow.setup = function()
    -- Force delete residual empty volatile tags
    capi.screen.connect_signal("tag::history::update", function(screen)
        for _, tag in ipairs(screen.tags) do
            if tag.volatile and not tag.selected then
                tag:delete()
            end
        end
    end)

    -- Enable titlebar on floating clients ONLY
    capi.client.connect_signal("property::floating", function(client)
        if client.floating then
            awful.titlebar.show(client)
        else
            awful.titlebar.hide(client)
        end
    end)

    -- Enable border for floating clients
    capi.client.connect_signal("property::floating", function(c)
        if c.floating then
            c.border_width = beautiful.border_width
        end
    end)

    -- No one maximize unless I said so
    local no_maximize = function(c)
        if c.maximized then
            c.maximized = false
            c.maximized_horizontal = false
            c.maximized_vertical = false
        end
    end
    capi.client.connect_signal("tagged", no_maximize)

    -- Enable sloppy focus, focus follows mouse movement
    capi.client.connect_signal("mouse::enter", function(c)
        c:emit_signal("request::activate", "mouse_enter", { raise = false })
    end)

    -- Focus/defocus border color, why isn't awesome do this itself?
    capi.client.connect_signal("focus", function(c)
        c.border_color = beautiful.border_focus
    end)
    capi.client.connect_signal("unfocus", function(c)
        c.border_color = beautiful.border_normal
    end)
end

return workflow
