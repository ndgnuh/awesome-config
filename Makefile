.PHONY: docs
docs-watch:
	find -name "*.lua" \
		-o -name "*.ld" \
		-o -name "*.ltp" \
		-o -name "*.css" \
		-o -name "*.md" \
		-o -name "Makefile" \
		| entr -rc make docs

docs:
	ldoc . --unsafe_no_sandbox --multimodule

docs-lunadoc:
	lunadoc -d public `find ndgnuh -name "*lua"` `find libs -name "*.lua"`
