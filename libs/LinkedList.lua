-- return a doubly linked list
local LinkedList = { mt = {} }
local NODE_TYPE = {} -- this is used to mark nods
local ic = require("libs.icecream")

local function Node(data)
    local node = { data = data, dtype = NODE_TYPE }

    function node:set_prev(node2)
        node2.next = self
        self.prev = node2
    end

    function node:set_next(node2)
        node2:set_prev(self)
    end

    node:set_prev(node)
    return node
end

LinkedList.Node = Node

--- Add item to the linked list at position i
-- i is optional, append if i is nil
function LinkedList:append(item, i)
    -- create node
    local node = Node(item)
    if self.data_hash then
        local hash = self.data_hash(item)
        self.by_hash[hash] = node
    end

    -- get tail node and attach to that
    local node_i = self:last_node()
    self:attach_at(node, node_i)
    return node
end

function LinkedList:foreach(callback)
    local node = self.first
    for _ = 0, 10 do -- prevent off by one
        node = node.next
        callback(node.data)
        if node == self.first then
            break
        end
    end
end

function LinkedList:get(i)
    -- other nodes
    local node = self.first
    for _ = 1, i do
        node = node.next
    end
    return node
end

--- Find node that have this data
function LinkedList:find(data)
    -- if hash by data is available
    -- return by hash
    if self.data_hash then
        local hash = self.data_hash(data)
        return self.by_hash[hash]
    end

    local node = self.first
    for _ = 1, 1000 do
        node = node.next
        if node.data == data then
            return node
        end
        if node == self.first then
            break
        end
    end

    -- not found
    return nil
end

--- Delete a node
-- node_p <--> node <--> node_n
-- to
-- node_p <--> node_n
function LinkedList:remove(node)
    local node_p = node.prev
    local node_n = node.next

    node_p.next = node_n
    node_n.prev = node_p
end

--- Attach a node after another node
-- from:
--  node_p <--> node <--> node_n
--  at_node <--> at_node_next
-- to:
--  node_p <--> node_n
--  at_node <--> node <--> at_node_next
-- edge case where node == at_node, the node will
-- be remove from the list entirely because the
-- at_node is removed and no node points to node
function LinkedList:attach_at(node, at_node)
    if node == at_node then
        return
    end

    -- perform node_p <--> node_n
    self:remove(node)

    local at_node_next = at_node.next
    node.prev = at_node
    node.next = at_node_next
    at_node.next = node
    at_node_next.prev = node
end

--- Remove all the node whose data
-- does not match predicate
function LinkedList:filter(predicate)
    local node = self.first
    repeat
        node = node.next
        if not predicate(node.data) then
            self:remove(node)
        end
    until node ~= self.first
end

--- Get last node
function LinkedList:last_node()
    return self.first.prev
end

function LinkedList:first_node()
    return self.first.next
end

--- check is empty
function LinkedList:is_empty()
    return self.first.prev == self.first
end

--- Get length
function LinkedList:len()
    local first = self.first

    local max_loop = 100
    local n = 0
    local node = first
    for _ = 1, max_loop do
        node = node.next
        if node == first then
            break
        end
        n = n + 1
    end
    return n
end

function LinkedList.mt:__call(args)
    local args = args or {}
    local ll = {
        first = LinkedList.Node("(FIRST NODE)"),
        data_hash = args.data_hash,
        by_hash = {},
    }


    for k, v in pairs(LinkedList) do
        if type(v) == "function" then
            ll[k] = v
        end
    end

    return ll
end

return setmetatable(LinkedList, LinkedList.mt)
