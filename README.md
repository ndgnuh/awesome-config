# My AwesomeWM configuration

## Features

## Installation

Install `awesome-git` for Debian:
```bash
cd /tmp
git clone https://github.com/awesomewm/awesome.git
cd awesome
sudo apt-get build-dep awesome -y
make package
sudo apt-get install build/*.deb
```

The folder `/tmp/awesome` won't be automatically cleaned.

## Tweaking

The directory structures:

- `rc.lua`: Main configuration files
- `libs`: Helper libraries, not specific to the configuration.
    - `libs.monkey`: Monkey patch directly to the `gears` module.
- `ndgnuh`: Heavily customized widgets, keybindings, popups and buttons.

## Tearing
