-- references:
-- - https://raw.githubusercontent.com/Aire-One/awesome-battery_widget/master/init.lua
-- - https://lazka.github.io/pgi-docs/UPowerGlib-1.0/enums.html#UPowerGlib.DeviceState
-- my custom upower widget
local wibox = require("wibox")
local base = require("wibox.widget.base")
local lgi = require("lgi")
local cairo = lgi.require("cairo")
local gears = require("gears")
local beautiful = require("beautiful")
local awful = require("awful")
local ic = require("libs.icecream")

local has_upower, UPowerGlib = pcall(lgi.require, "UPowerGlib")
local upower = { mt = {} }

local device_state_to_str = {
    [UPowerGlib.DeviceState.PENDING_DISCHARGE] = "Discharging",
    [UPowerGlib.DeviceState.PENDING_CHARGE]    = "Charging",
    [UPowerGlib.DeviceState.FULLY_CHARGED]     = "Full",
    [UPowerGlib.DeviceState.EMPTY]             = "Empty",
    [UPowerGlib.DeviceState.DISCHARGING]       = "Discharging",
    [UPowerGlib.DeviceState.CHARGING]          = "Charging",
    [UPowerGlib.DeviceState.UNKNOWN]           = "Unknown"
}

function upower:set_state(state)
    self._private.state = state
    self:emit_signal("widget::redraw_needed")
end

function upower:get_state()
    return self._private.state or { charging = false, percentage = 0 }
end

function upower:draw(_, cr, w, _)
    local h        = w / 1.618 -- height
    local cw       = w * 0.1   -- horizontal cut out
    local ch       = h / 5     -- horizontal cut out
    local bx       = w - cw    -- width - cutout
    local charging = self:get_charging()
    local fc       = (charging and "#23ae32" or "#1d1f21")
    local ec       = (charging and "#81c784" or "#aaaaaa")
    local pct      = self:get_percentage()

    cr:rectangle(0, 0, w, h)
    cr:set_source(gears.color("#ff0000"))
    cr:fill()

    -- create picture
    cr:translate(0, (w - h) / 2)

    -- the battery shape
    cr:move_to(0, 0)
    cr:rel_line_to(bx, 0)
    cr:rel_line_to(0, ch)
    cr:rel_line_to(cw, 0)
    cr:rel_line_to(0, h - ch - ch)
    cr:rel_line_to(-cw, 0)
    cr:line_to(bx, h - 2)
    cr:line_to(0, h - 2)
    cr:close_path()
    cr:clip()

    -- the empty container
    cr:rectangle(0, 0, w, h)
    cr:set_source(gears.color(ec))
    cr:fill()

    -- battery juice
    cr:move_to(0, 0)
    cr:rectangle(0, 0, w * pct, h)
    cr:set_source(gears.color(fc))
    cr:fill()

    -- the lightning bolt will have to wait
    -- use a plus sign for now
    if charging then
        local r = h / 6
        cr:move_to(bx / 2, h / 2)
        cr:rel_move_to(-0.5 * r, -1.5 * r)
        cr:rel_line_to(r, 0)
        cr:rel_line_to(0, r)
        cr:rel_line_to(r, 0)
        cr:rel_line_to(0, r)
        cr:rel_line_to(-r, 0)
        cr:rel_line_to(0, r)
        cr:rel_line_to(-r, 0)
        cr:rel_line_to(0, -r)
        cr:rel_line_to(-r, 0)
        cr:rel_line_to(0, -r)
        cr:rel_line_to(r, 0)
        cr:rel_line_to(0, -r)
        cr:set_source(gears.color("#eee"))
        cr:fill()
    end
end

local function seconds_to_time(seconds)
    if seconds <= 0 then
        return "00:00:00";
    else
        local hours = string.format("%02.f", math.floor(seconds / 3600));
        local mins = string.format("%02.f", math.floor(seconds / 60 - (hours * 60)));
        local secs = string.format("%02.f", math.floor(seconds - hours * 3600 - mins * 60));
        return hours .. ":" .. mins .. ":" .. secs
    end
end


local function redraw(self)
    local percentage = self._private.percentage
    local charging = self._private.charging
    local device = self._private.device
    local tooltip = self._private.tooltip
    local seconds_left = seconds_to_time(
        charging and device.time_to_full or device.time_to_empty)

    -- update the widget
    local text = self:get_children_by_id("text_role")[1]
    text:set_text(tostring(math.floor(percentage * 100)) .. "%")

    local icon = self:get_children_by_id("icon_role")[1]
    icon:set_image(battery_icon {
        charging = charging,
        percentage = percentage
    })

    -- update the tooltip
    local tooltip_text = ("∘ Percentage: "
        .. tostring(math.floor(percentage * 100)) .. "%"
        .. "\n"
        .. "∘ Time to " .. (charging and "full" or "empty") .. ": "
        .. seconds_left
        .. "\n"
        .. "∘ Device status: " .. tostring(device_state_to_str[device.state]))
    tooltip:set_text(tooltip_text)
end

function upower:set_percentage(percentage)
    self._private.percentage = percentage
    self:emit_signal("widget::redraw_needed")
    self:emit_signal("property::percentage", percentage)
end

function upower:set_charging(charging)
    self._private.charging = charging
    self:emit_signal("widget::redraw_needed")
    self:emit_signal("property::charging")
end

function upower:get_percentage()
    return self._private.percentage
end

function upower:get_charging()
    return self._private.charging
end

function upower:fit(ctx, w, h)
    local ret = wibox.layout.stack.fit(self, ctx, w, h)
    return ret
end

local new = function()
    -- widget template
    -- local spacing = beautiful.wibar_spacing
    -- local widget_template = {
    --     widget = wibox.container.margin,
    --     left = spacing,
    --     right = spacing,
    --     {
    --         layout = wibox.layout.fixed.horizontal,
    --         spacing = spacing / 2,
    --         {
    --             widget = wibox.widget.imagebox,
    --             image = battery_icon({ percentage = 0.0 }),
    --             id = "icon_role",
    --         },
    --         {
    --             widget = wibox.widget.textbox,
    --             markup = "50%",
    --             id = "text_role",
    --         },
    --     }
    -- }

    -- make widget
    local ret = base.make_widget()
    gears.table.crush(ret, upower)

    -- local tt = awful.tooltip()
    -- ret._private.tooltip = tt
    -- tt:set_text("Can't update the widget, maybe upower is not installed")
    -- tt:add_to_object(ret)

    -- if has_upower then
    --     local device = UPowerGlib.Client():get_display_device()
    --     ret._private.device = device
    --     local function update()
    --         local percentage = device.percentage / 100
    --         local state = device.state
    --         local charging = (
    --             (state == UPowerGlib.DeviceState.PENDING_CHARGE) or
    --             (state == UPowerGlib.DeviceState.CHARGING)
    --         )
    --         ret:set_percentage(percentage)
    --         ret:set_charging(charging)
    --     end
    --     update()
    --
    --     device.on_notify = update
    -- else
    --     ret._private.device = false
    -- end

    return ret
end

function upower.mt:__call(...)
    return new(...)
end

return setmetatable(upower, upower.mt)
