local m = {}
local lgi = require("lgi")
local Gtk = lgi.require("Gtk", "3.0")
local GObject = lgi.require("GObject", "2.0")
local ic = require("libs.icecream")

--- run xrandr and get screen configurations
function m.xrandr()
    local stdout = io.popen("xrandr"):read("*a")
    local ret = { screens = {}, connected_screens = {}, primary_screen = false }

    -- information won't be parsed to number,
    -- as they would be converted to string later anyway
    local matches, screen
    for line in string.gmatch(stdout, "[^\n]+") do
        -- match connected/disconnected screen
        -- due to the limit of lua pattern, we can't capture everything in one go
        -- if not a line dedicated to string, parse mode instead
        matches = { string.match(line, "^([A-Za-z0-9-]+) (connected)") }
        matches = #matches > 0 and matches or { string.match(line, "^([A-Za-z0-9-]+) (disconnected)") }

        -- noscreen found, parse mode instead
        if #matches == 0 then
            local w, h, freq, active = string.match(line, "(%d+)x(%d+)%s+([0-9.]+)[ *]*([+]*)")
            if w then
                assert(screen ~= nil, "Mode line found but screen is nil")
                active = active == "+"
                local mode = { width = w, height = h, refresh_rate = freq, active = active }
                table.insert(ret[screen].modes, mode)
                if active then
                    ret[screen].current = mode
                end
            end
        else
            -- else, parse screen
            -- first, collect extra informations
            screen = matches[1]
            local connected = matches[2] == "connected"
            local primary = (line:match("(primary)") ~= nil) or false
            local geometry = false
            if connected then
                table.insert(ret.connected_screens, screen)
                local w, h, x, y = line:match("(%d+)x(%d+)%+(%d+)%+(%d+)")
                if w and h and x and y then
                    geometry = { x = x, y = y, width = width, height = height }
                end
            end
            if primary then
                ret.primary_screen = screen
            end

            -- register screens
            table.insert(ret.screens, screen)
            ret[screen] = {
                id = screen,
                connected = connected,
                primary = primary,
                geometry = geometry,
                modes = {},
                current_mode = false,
            }
        end
    end

    return ret
end

function m.show()
end

local function mainwindow()
    local win = Gtk.Window { title = "Display configuration" }
    local screens = m.xrandr()

    -- https://docs.gtk.org/gdk3/enum.WindowTypeHint.html
    win:set_type_hint(3) -- toolbar

    -- mainlayout
    local lay = Gtk.Grid()
    win:add(lay)

    -- checkbox for which screen to use
    local checklist = Gtk.Box { orientation = 1 }
    do
        checklist:add(Gtk.Label { label = "Displays", id = "mylabel" })
        for _, screen in ipairs(screens.screens) do
            local checkbox = Gtk.CheckButton.new_with_label(screen)
            checkbox:set_active(screens[screen].connected)
            checklist:add(checkbox)
        end
        lay:attach(checklist, 1, 1, 1, 1)
    end

    -- checkbox for primary screen screen to use
    local lay2 = Gtk.Box { orientation = 1 }
    local primary_select = require("libs.gtk.combobox") {
        items = screens.screens,
        callback = function(i, items)
            ic(i)
        end
    }
    do
        lay2:add(Gtk.Label({ label = "Primary screen" }))
        lay2:add(primary_select)
        lay:attach(lay2, 2, 1, 1, 1)
    end


    local refresh = Gtk.Button { label = "Refresh" }
    local save = Gtk.Button { label = "Save" }
    function save:on_clicked(self)
        ic()
    end

    lay:attach(refresh, 1, 2, 3, 1)
    lay:attach(save, 1, 3, 3, 1)
    return win
end

local w = mainwindow()
w:show_all()
